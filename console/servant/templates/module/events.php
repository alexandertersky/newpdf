<?php 

use App\Helpers\Event;

Event::addListener('render_navigation', function ($event, &$items) {
    $items[0][] = (array)json_decode('${item}');
});