$app->group('/admin', function() use($app, $c) {

    $app->get('/${low_route}s', ${route}Controller::class . ':showAdmin${route}List')->setName('${low_route}.showAdmin${route}List')->add(new Access($c, ['${low_route}s_view']));
    $app->get('/${low_route}s/add', ${route}Controller::class . ':showAdmin${route}Add')->setName('${low_route}.showAdmin${route}Add')->add(new Access($c, ['${low_route}s_create']));
    $app->get('/${low_route}s/{id}', ${route}Controller::class . ':showAdmin${route}Edit')->setName('${low_route}.showAdmin${route}Edit')->add(new Access($c, ['${low_route}s_update']));

})->add(new Registred($container));