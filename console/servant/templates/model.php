<?php

namespace App\Models;

class ${class_name} extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = '${table_name}';
    protected $primaryKey = '${primary_key}';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [${casts}];

}