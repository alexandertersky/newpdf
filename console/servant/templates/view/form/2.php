
                    </div>

                </div>

            </div>

        </div>
        <div class="row" style="margin-top: 15px">
            <div class="col-md-4">
                <button class="btn btn-outline-primary" type="submit">
                    Сохранить <i class="icon-attachment ml-2"></i>
                </button>
            </div>

            <div class="col-md-4">
                {% if ${class_name_low}.id %}
                <button id="delete-some" class="btn btn-outline-danger" type="button">
                    Удалить //--//--// <i class="icon-bin2 ml-2"></i>
                </button>
                {% endif %}
            </div>
        </div>

    </form>

    <form id="frm-del" action="{{ path_for('${class_name_low}.delete${class_name}', {'id': ${class_name_low}.id}) }}" method="get">
    </form>
{% endblock %}

{% block script %}

<script type="text/javascript">
    $(function() {
        $('#delete-some').click(function() {
            if (confirm('Удалить?')) {
                $('#frm-del').submit();
            }
        });
    });
</script>
{% endblock %}