<?php

namespace App\Models;

class Taggable extends CustomModel
{
    public $timestamps = false;
    protected $table = 'taggables';
    protected $primaryKey = 'id';
}