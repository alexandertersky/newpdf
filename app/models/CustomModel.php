<?php

namespace App\Models;

use App\Helpers\FilesTrait;
use Illuminate\Database\Eloquent\Model;

class CustomModel extends Model
{
    use FilesTrait;
    public function update(array $attributes = [], array $options = [])
    {
        foreach ($this->checkbox as $item){
            if (!array_key_exists($item, $attributes)){
                $attributes[$item] = false;
            }
        }
        return parent::update($attributes, $options);
    }

}