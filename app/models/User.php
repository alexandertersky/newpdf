<?php

namespace App\Models;

use App\Helpers\Event;
use App\Helpers\FilesTrait;
use Cartalyst\Sentinel\Users\EloquentUser;

class User extends EloquentUser
{
    use FilesTrait;

    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'id' => 'integer',
		'email' => 'string',
		'password' => 'string',
		'permissions' => 'text',
		'last_login' => 'datetime',
		'first_name' => 'string',
		'last_name' => 'string',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
	];
    protected $fillable = [];

    public function __get($key)
    {
        $relations = [];
        Event::trigger('user_relations', $relations);

        foreach ($relations as $relation){
            if ($key === $relation['name']){
                return $relation['closure']->call($this)->get();
            }
        }
        return parent::__get($key);
    }

    public function __call($method, $parameters)
    {
        $relations = [];
        Event::trigger('user_relations', $relations);

        foreach ($relations as $relation){
            if ($method === $relation['name']){
                return $relation['closure']->call($this);
            }
        }
        return parent::__call($method, $parameters);
    }

    public function role() {
        return $this->belongsToMany(Role::class, 'role_users', 'user_id', 'role_id');
    }

    public function photo()
    {
        return $this->hasOne(File::class, 'id', 'photo_id');
    }

    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }
}