<?php

namespace App\Models;

class Tag extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'tags';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'id' => 'integer',
		'title' => 'string',
	];

    public function banners()
    {
        return $this->morphedByMany(Banner::class, 'taggable');
    }

    public function messages()
    {
        return $this->morphedByMany(Message::class, 'taggable');
    }

}