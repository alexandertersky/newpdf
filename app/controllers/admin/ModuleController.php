<?php
namespace App\Controllers\Admin;

use App\Controllers\Controller;
use App\Models\Module;
use App\Helpers\ModulesSingleton;

class ModuleController extends Controller
{
    public function showAdminModuleList($request, $response, $args)
    {
        $ms = ModulesSingleton::getInstance();
        $mods_list = $ms->getModules();
        $installed = Module::get()->keyBy('title')->toArray();

        $modules = array_merge($mods_list, $installed);

        $this->twig_vars['modules'] = $modules;
        $this->render('admin/modules/modules-list.twig');
    }

    public static function appModules()
    {
        $modules_dirs = glob(__DIR__.'/../modules/*', GLOB_ONLYDIR);
        $modules = [];

        foreach($modules_dirs as $modules_dir)
        {
            $files = glob($modules_dir.'/*.*');
            $convention = file_get_contents($modules_dir.'/convention.json');
            $convention = json_decode($convention, true);
            $modules[]= [
                'title' => $convention['ModuleName'],
                'status' => false,
                'path' => $modules_dir,
                'convention' => json_encode($convention)
            ];
        }

        return $modules;
    }

   public function switchModule($request, $response, $args)
    {
        $data = $request->getParams();
        $module = Module::find($data['id']);
        $module->status = $data['status'] == 0? 1: 0;
        $module->save();
    }

    public function addModule($request)
    {
        $module = $request->getParams()['module'];
        $ms = ModulesSingleton::getInstance();
        $ms->getModule($module)->install();
    }

    public function removeModule($request)
    {
        $module = $request->getParams()['module'];
        $ms = ModulesSingleton::getInstance();
        $ms->getModule($module)->uninstall();
    }

    public function getModuleJs($request, $response, $args)
    {
        header('Content-Type: application/javascript');

        $module_name = $args['module_name'];
        $filename = $args['filename'];

        $ms = ModulesSingleton::getInstance();
        $script = $ms->getModule($module_name)->getScript($filename);

        die($script);
    }

    public function getModuleCss($request, $response, $args)
    {
        header('Content-Type: text/css');

        $module_name = $args['module_name'];
        $filename = $args['filename'];

        $ms = ModulesSingleton::getInstance();
        $style = $ms->getModule($module_name)->getStyle($filename);

        die($style);
    }

    public function getModuleImg($request, $response, $args)
    {
        $module_name = $args['module_name'];
        $filename = $args['filename'];

        $ext = pathinfo($filename, PATHINFO_EXTENSION);

        header('Content-Type: image/' . $ext);

        $ms = ModulesSingleton::getInstance();
        $img = $ms->getModule($module_name)->getImage($filename);

        die($img);
    }

}