<?php

namespace App\Controllers\Admin;

use App\Controllers\Controller;
use App\Models\Role;
use App\Models\Module;

class RoleController extends Controller
{
    public function showAdminRoleList($request, $response, $args)
    {
        $roles = Role::all();
        $this->twig_vars['roles'] = $roles;

        $this->twig_vars['permissions'] = Module::get()->keyBy('title')->map(function($c) {
            if (property_exists($c->convention, 'Permissions')) {
                return (array)$c->convention->Permissions;
            }
        })->filter()->toArray();

        return $this->render('admin/users/user-roles.twig');
    }

    public function getRole($request, $response, $args) {
        $id = $request->getParams()['id'];
        echo Role::find($id);
    }

    public function createUserRoles($request, $response, $args) {
        $data = $request->getParams();

        $data = array_filter($data);

        $data['permissions'] = isset($data['permissions']) ? $data['permissions'] : [];
        $data['permissions'] = json_encode(array_fill_keys($data['permissions'],true));

        if (isset($data['id'])) Role::whereId($data['id'])->update($data);
        else Role::create($data);

        return $response->withStatus(301)->withHeader('Location', '/admin/roles');
    }

}