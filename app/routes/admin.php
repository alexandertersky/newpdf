<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Controllers\Admin\UserController;
use App\Controllers\Admin\RoleController;
use App\Middleware\Access;
use App\Middleware\Registred;
use App\Controllers\Admin\NavigationController;
use App\Controllers\Admin\NavigationItemController;
use App\Controllers\Admin\ModuleController;
//delimiter//

$c = $app->getContainer();

$app->get('/', function (Request $request, Response $response) use ($c) {
    return $this->view->render($response, 'admin/layout/layout_new.twig');
})->add(new Registred($c));



$app->get('/message/no-access', UserController::class.':noAccess')->setName('error.noAccess');


$app->get('/modules/{module_name}/assets/css/{filename}', ModuleController::class . ':getModuleCss')->setName('module.css');
$app->get('/modules/{module_name}/assets/js/{filename}', ModuleController::class . ':getModuleJs')->setName('module.js');
$app->get('/modules/{module_name}/assets/img/{filename}', ModuleController::class . ':getModuleImg')->setName('module.img');

$app->group('/admin', function() use($app, $c) {

    $app->get('/users', UserController::class . ':showAdminUserList')->setName("user.showAdminUserList")->add(new Access($c, ['user_view']));
    $app->get('/users/add', UserController::class . ':showAdminUserAdd')->setName("user.showAdminUserAdd")->add(new Access($c, ['user_view', 'user_create']));
    $app->get('/users/{id}', UserController::class . ':showAdminUserEdit')->setName('user.showAdminUserEdit')->add(new Access($c, ['user_view', 'user_update']));

    $app->get('/roles', RoleController::class . ':showAdminRoleList')->setName('role.showAdminRoleList')->add(new Access($c, ['roles_view']));

    $app->get('/navigation', NavigationController::class . ':showAdminNavigationList')->setName('navigation.showAdminNavigationList');
    $app->get('/navigation/add', NavigationController::class . ':showAdminNavigationAdd')->setName('navigation.showAdminNavigationAdd');
    $app->get('/navigation/{id}', NavigationController::class . ':showAdminNavigationEdit')->setName('navigation.showAdminNavigationEdit');

    $app->get('/navigation/{id}/items', NavigationItemController::class.':showadminNavigationItemList')->setName('navigation.showAdminNavigationItemList');
    $app->get('/navigation/{id}/items/add', NavigationItemController::class.':showAdminNavigationItemAdd')->setName('navigation.showAdminNavigationItemAdd');
    $app->get('/navigation/{id}/items/{item_id}', NavigationItemController::class.':showAdminNavigationItemEdit')->setName('navigation.showAdminNavigationItemEdit');

    $app->get('/modules', ModuleController::class . ':showAdminModuleList')->setName('module.showAdminModuleList')->add(new Access($c, ['modules_view']));

})->add(new Registred($c));