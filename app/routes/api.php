<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Controllers\Admin\UserController;
use App\Controllers\Admin\RoleController;
use App\Middleware\Registred;
use App\Middleware\Access;
use App\Controllers\Admin\NavigationController;
use App\Controllers\Admin\NavigationItemController;
use App\Controllers\Admin\ModuleController;
//delimiter//

$app->get('/login', UserController::class.':showLoginPage')->setName('user.showLoginPage');
$app->post('/api/login', UserController::class.':doLogin')->setName('user.doLogin');
$app->get('/api/logout', UserController::class.':doLogout')->setName('user.doLogout');

$app->group('/api', function() use($app, $c) {

    $app->post('/users', UserController::class . ':createUser')->setName('user.createUser')->add(new Access($c, ['user_create']));
    $app->post('/users/{id}', UserController::class . ':updateUser')->setName('user.updateUser')->add(new Access($c, ['user_update']));
    $app->post('/users/{id}/delete', UserController::class . ':deleteUser')->setName('user.deleteUser')->add(new Access($c, ['user_delete']));

    $app->post('/users/{id}/remove-avatar', UserController::class.':deleteUserAvatar')->setName('user.deleteAvatar')->add(new Access($c, ['user_update']));;

    $app->post('/roles', RoleController::class . ':createRole')->setName('role.createRole')->add(new Access($c, ['roles_view']));
    $app->post('/roles/get_role', RoleController::class . ':getRole')->setName('role.getRole')->add(new Access($c, ['roles_view']));
    $app->post('/roles/create', RoleController::class . ':createUserRoles')->setName('role.createRole')->add(new Access($c, ['roles_view']));

    $app->post('/navigation/items/save', NavigationItemController::class.':saveNavigationItems')->setName('navigation.saveNavigationItems');
    $app->post('/navigation/get-item-permissions', NavigationItemController::class.':getItemPermissions')->setName('navigation.getItemPermissions');

    $app->post('/navigation', NavigationController::class.':createNavigation')->setName('navigation.createNavigation');
    $app->post('/navigation/{id}', NavigationController::class.':updateNavigation')->setName('navigation.updateNavigation');
    $app->get('/navigation/{id}/delete', NavigationController::class.':deleteNavigation')->setName('navigation.deleteNavigation');

    $app->post('/navigation/{id}/item', NavigationItemController::class.':createNavigationItem')->setName('navigation.createNavigationItem');
    $app->post('/navigation/{id}/item/{item_id}', NavigationItemController::class.':updateNavigationItem')->setName('navigation.updateNavigationItem');
    $app->get('/navigation/{id}/item/{item_id}/delete', NavigationItemController::class.':deleteNavigationItem')->setName('navigation.deleteNavigationItem');

    $app->post('/modules/switch-module', ModuleController::class.':switchModule')->setName('module.switchModule');
    $app->post('/modules/add-module', ModuleController::class.':addModule')->setName('module.addModule');
    $app->post('/modules/remove-module', ModuleController::class.':removeModule')->setName('module.removeModule');

})->add(new Registred($container));