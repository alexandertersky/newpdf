<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use Tracy\Debugger;
use App\Middleware\Pagination;
use Psr7Middlewares\Middleware;
use App\Helpers\TwigCustomExtensions;
use App\Helpers\ModulesSingleton;

session_start();

require __DIR__ . '/../vendor/autoload.php';

$dotenv = new \Dotenv\Dotenv(dirname(__DIR__.'/../configs/.env'));
$dotenv->load();

// Eloquent
require __DIR__.'/../configs/config.php';
$capsule = new Capsule;
$capsule->addConnection($db);
$capsule->setEventDispatcher(new Dispatcher(new Container));
$capsule->setAsGlobal();
$capsule->bootEloquent();

try{
    $capsule->getConnection()->getPdo();
} catch (\Doctrine\DBAL\Driver\PDOException $e){
    exit('Соединение с бд не установлено');
}

// Create app
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);

// Get container
$container = $app->getContainer();

// Add View to Container
$container['view'] = function($container) {
    $view = new \Slim\Views\Twig([__DIR__.'/views']);
    $view->addExtension(new TwigCustomExtensions($container));
    $view->addExtension(new Slim\Views\TwigExtension(
        $container->router,
        $container->request->getUri()
    ));

    $twig = $view->getEnvironment();
    $twig->addGlobal("session", $_SESSION);
    $filter = new \Twig\TwigFilter('customFilter', function ($statement, $args) use ($twig){
        foreach ($args as $filter){
            if ($filter['name'] !== 'property'){
                if (isset($filter['args'])){
                    $statement = call_user_func_array($twig->getFilter($filter['name'])->getCallable(), [$twig, $statement, $filter['args']]);
                } else {
                    $statement = call_user_func_array($twig->getFilter($filter['name'])->getCallable(), [$twig, $statement]);
                }
            } else {
                if (is_array($statement)){
                    $statement = $statement[$filter['args'][0]];
                } else {
                    $statement = $statement ->{$filter['args'][0]};
                }
            }
        }
        return $statement;
    });
    $twig->addFilter($filter);
    return $view;
};

//Add Sentinel to Container
if (file_exists(__DIR__.'/../configs/config.php')) {
    $container['sentinel'] = function ($container) {
        $sentinel = (new \Cartalyst\Sentinel\Native\Facades\Sentinel())->getSentinel();
        $sentinel->getUserRepository()->setModel(\App\Models\User::class);
        $sentinel->getPersistenceRepository()->setUsersModel(\App\Models\User::class);
        return $sentinel;
    };
}

//Add Flash to Container
$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};

$container['emitter'] = function()
{
    $emitter = new \League\Event\Emitter();
    return $emitter;
};

/*INCLUDING MIDDLEWARES*/
// To pagination right work
$app->add( new Pagination());

//Remove the last slash from the URL
$app->add(Middleware::trailingSlash()->redirect(301));
/*---------------------*/

//Init Config
$config = \App\Helpers\Config::getInstance($container);

//Tracy Debugger
Debugger::enable(false);

date_default_timezone_set('Asia/Krasnoyarsk');

require __DIR__.'/routes/admin.php';
require __DIR__.'/routes/api.php';

$mods_installed = \App\Models\Module::where('status', 1)->get()->keyBy('title');
$ms = ModulesSingleton::getInstance($container);

foreach ($mods_installed->getIterator() as $mod) {

    // check requirements
    $enabled = true;

    if (property_exists($mod->convention, 'Requirements')) {
        
        foreach ($mod->convention->Requirements as $requirement) {
            if (array_key_exists($requirement, $mods_installed->toArray()) == false) {
                $enabled = false;
                break;
            }
        }
    }

    if (!$enabled) continue;

    if ($module = $ms->getModule($mod->title)) {
        foreach ($module->getRoutes() as $route) {
            include_once realpath($route);
        }

        $module->run();
    }
}