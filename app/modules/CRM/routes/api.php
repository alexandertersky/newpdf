<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Middleware\Access;
use App\Middleware\Registred;

use App\Modules\CRM\Controllers\LeadController;
use App\Modules\CRM\Controllers\PersonController;
use App\Modules\CRM\Controllers\CompanyController;
use App\Modules\CRM\Controllers\TagController;

$c = $app->getContainer();

$app->group('/api', function() use($app, $c) {

    $app->post('/leads/group/create', LeadController::class.':createLeadGroup')
        ->setName('lead.createGroup')
        ->add(new Access($c, ['leads_update']));

    $app->post('/leads/group/{id}', LeadController::class.':updateLeadGroup')
        ->setName('lead.updateGroup')
        ->add(new Access($c, ['leads_update']));

    $app->delete('/leads/group/{id}', LeadController::class.':deleteLeadGroup')
        ->setName('lead.deleteGroup')
        ->add(new Access($c, ['leads_update']));

	$app->post('/leads', LeadController::class.':createLead')
		->setName('lead.createLead')
		->add(new Access($c, ['leads_create']));

    $app->post('/leads/import', LeadController::class.':importLead')
        ->setName('lead.import')
        ->add(new Access($c, ['leads_create']));

    $app->post('/leads/{id}', LeadController::class.':updateLead')
    	->setName('lead.updateLead')
    	->add(new Access($c, ['leads_update']));

    $app->delete('/leads/{id}', LeadController::class.':deleteLead')
    	->setName('lead.deleteLead')
    	->add(new Access($c, ['leads_delete']));

    $app->post("/tags", TagController::class.":createTag")
    	->setName("tag.createTag")
    	->add(new Access($c, ['tags_create']));

    $app->post('/persons', PersonController::class.':createPerson')
    	->setName('person.createPerson')
    	->add(new Access($c, ['persons_create']));

    $app->post('/persons/send-mail', PersonController::class.':sendMailByTags')
    	->setName('person.sendMainByTags')
    	->add(new Access($c, ['persons_create']));

    $app->post('/persons/{id}', PersonController::class.':updatePerson')
    	->setName('person.updatePerson')
    	->add(new Access($c, ['persons_update']));

    $app->delete('/persons/{id}', PersonController::class.':deletePerson')
    	->setName('person.deletePerson')
    	->add(new Access($c, ['persons_delete']));

    $app->get("/persons/createFromLead/{id}", PersonController::class.":createPersonFromLead")
    	->setName('person.createPersonFromLead')
    	->add(new Access($c, ["persons_create"]));

    $app->post('/companys', CompanyController::class.':createCompany')
    	->setName('company.createCompany')
    	->add(new Access($c, ['companys_create']));

    $app->post('/companys/{id}', CompanyController::class.':updateCompany')
    	->setName('company.updateCompany')
    	->add(new Access($c, ['companys_update']));

    $app->delete('/companys/{id}', CompanyController::class.':deleteCompany')
    	->setName('company.deleteCompany')
    	->add(new Access($c, ['companys_delete']));

    $app->get('/company/{id}/deleteLogo', CompanyController::class.':deleteLogo')
    	->setName('company.deleteLogo')
    	->add(new Access($c, ['companys_delete']));
})->add(new Registred($container));