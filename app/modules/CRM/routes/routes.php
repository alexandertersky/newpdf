<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Middleware\Access;
use App\Middleware\Registred;

use App\Modules\CRM\Controllers\LeadController;
use App\Modules\CRM\Controllers\PersonController;
use App\Modules\CRM\Controllers\CompanyController;
use App\Modules\CRM\Controllers\TagController;

$c = $app->getContainer();

$app->group('/admin', function() use($app, $c) {

    $app->get('/leads', LeadController::class . ':showAdminLeadList')
	    ->setName('lead.showAdminLeadList')
	    ->add(new Access($c, ['leads_view']));

    $app->get('/leads/add', LeadController::class . ':showAdminLeadAdd')
    	->setName('lead.showAdminLeadAdd')
    	->add(new Access($c, ['leads_create']));

    $app->get('/leads/{id}', LeadController::class . ':showAdminLeadEdit')
    	->setName('lead.showAdminLeadEdit')
    	->add(new Access($c, ['leads_update']));


    $app->get('/companys', CompanyController::class . ':showAdminCompanyList')
    	->setName('company.showAdminCompanyList')
    	->add(new Access($c, ['companys_view']));

    $app->get('/companys/add', CompanyController::class . ':showAdminCompanyAdd')
    	->setName('company.showAdminCompanyAdd')
    	->add(new Access($c, ['companys_create']));

    $app->get('/companys/{id}', CompanyController::class . ':showAdminCompanyEdit')
    	->setName('company.showAdminCompanyEdit')
    	->add(new Access($c, ['companys_update']));


    $app->get('/companys/{id}/showRequisites', CompanyController::class.':showAdminRequisitesEdit')
    	->setName('company.showAdminRequisites')
    	->add(new Access($c,['companys_update']));

    $app->get('/persons', PersonController::class . ':showAdminPersonList')
    	->setName('person.showAdminPersonList')
    	->add(new Access($c, ['persons_view']));

    $app->get('/persons/add', PersonController::class . ':showAdminPersonAdd')
    	->setName('person.showAdminPersonAdd')
    	->add(new Access($c, ['persons_create']));

    $app->get('/persons/send-mail', PersonController::class.':showAdminSendMail')
    	->setName('person.showAdminPersonMailer')
    	->add(new Access($c, ['persons_view']));

    $app->get('/persons/{id}', PersonController::class . ':showAdminPersonEdit')
    	->setName('person.showAdminPersonEdit')
    	->add(new Access($c, ['persons_update']));
})->add(new Registred($c));