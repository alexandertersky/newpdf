<?php

namespace App\Modules\CRM\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Wtolk\Eloquent\Filter;
use App\Models\CustomModel;

class Person extends CustomModel
{

    use Filter;
    use SoftDeletes;

    protected $guarded = [];
    protected $table = 'persons';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];

    public function phones()
    {
        return $this->morphToMany("App\Modules\CRM\Models\Phone", "phonable");
    }

    public function company()
    {
        return $this->belongsTo("App\Modules\CRM\Models\Company");
    }

    public function tags()
    {
        return $this->morphToMany("App\Models\Tag", "taggable");
    }

    public function setBirthdayAttribute($value)
    {
        if ($value == ''){
            $this->attributes['birthday'] = null;
        }
    }

    public function phones_filter($value, $builder)
    {
        $value = str_replace(['+','(', ')','-', ' ', '—'], '', $value);
        $builder->whereHas('phones', function ($query) use ($value){
            $query->where('phone', 'like', '%'.$value.'%');
        });
        return $builder;
    }

    public function tags_filter($value, $builder)
    {
        $builder->whereHas('tags', function ($query) use ($value){
            $query->whereIn('tags.id',$value);
        });
        return $builder;
    }

    public function company_filter($value, $builder)
    {
        return $builder->whereHas('company', function ($query) use ($value){
            $query->where('title', 'like', '%'.$value.'%');
        });
    }

}