<?php

namespace App\Modules\CRM\Models;

use App\Models\CustomModel;

class Record extends CustomModel
{
    protected $guarded = [];
    protected $table = 'records';
    protected $primaryKey = 'id';
    public $timestamps = false;
}