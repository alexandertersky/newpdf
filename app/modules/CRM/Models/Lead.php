<?php

namespace App\Modules\CRM\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Wtolk\Eloquent\Filter;
use App\Models\CustomModel;

class Lead extends CustomModel
{
    use Filter;
    use SoftDeletes;

    protected $guarded = [];
    protected $table = 'leads';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'comment' => 'text',
        'email' => 'string',
        'company' => 'string',
        'source' => 'text',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
    ];

    public function phones()
    {
        return $this->morphToMany("App\Modules\CRM\Models\Phone", "phonable");
    }

    public function records()
    {
        return $this->morphToMany("App\Modules\CRM\Models\Record", 'recorded', 'recorded');
    }

    public function tags()
    {
        return $this->morphToMany("App\Models\Tag", "taggable");
    }

    public function setBirthdayAttribute($value)
    {
        if ($value == ''){
            $this->attributes['birthday'] = null;
        }
    }

    public function phones_filter($value, $builder)
    {
        $value = str_replace(['+','(', ')','-', ' ', '—'], '', $value);
        $builder->whereHas('phones', function ($query) use ($value){
            $query->where('phone', 'like', '%'.$value.'%');
        });
        return $builder;
    }

    public function tags_filter($value, $builder)
    {
        $builder->whereHas('tags', function ($query) use ($value){
            $query->whereIn('tags.id',$value);
        });
        return $builder;
    }

}