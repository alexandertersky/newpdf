<?php

namespace App\Modules\CRM\Models;

use App\Models\CustomModel;

class CRMGroup extends CustomModel
{
    protected $guarded = [];
    protected $table = 'crm_groups';
    protected $primaryKey = 'id';
    public $timestamps = false;


    public function leads()
    {
        return $this->hasMany("App\Modules\CRM\Models\Lead", 'crm_group_id', 'id');
    }
}