<?php

namespace App\Modules\CRM\Models;

use App\Models\CustomModel;

class Phone extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'phones';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'id' => 'integer',
		'phone' => 'string',
		'phonable_id' => 'integer',
		'phonable_type' => 'string',
	];

    public function leads()
    {
        return $this->morphedByMany("App\Modules\CRM\Models\Lead", "phonable");
    }

    public function persons()
    {
        return $this->morphedByMany("App\Modules\CRM\Models\Person", "phonable");
    }

    public function setPhoneAttribute($value)
    {
        $value = str_replace(['+','(', ')','-', ' ', '—'], '', $value);
        $this->attributes['phone'] = $value;
    }

    public function getFormatedPhoneAttribute()
    {
        $phone = substr($this->phone, -10);

        return sprintf('+7(%s)%s-%s-%s',
            substr($phone, 0,3),
            substr($phone, 3,3),
            substr($phone, 6,2),
            substr($phone, 8,2));
    }
}