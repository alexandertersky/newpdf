<?php

namespace App\Modules\CRM\Models;

use Wtolk\Eloquent\Filter;
use App\Models\CustomModel;

class Company extends CustomModel
{

    use Filter;

    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'companies';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'id' => 'integer',
		'title' => 'text',
		'requisites' => 'array',
        'representative_header' => 'text',
        'representative_footer' => 'string',
        'logo_id' => 'integer'
	];

    public function tags()
    {
        return $this->morphToMany("App\Models\Tag", "taggable");
    }

    public function logo()
    {
        return $this->hasOne("App\Models\File", 'id', 'logo_id');
    }

    public function search($value, $builder){
        return $builder->where('name', 'like', '%'.$value.'%')->orWhere('title', 'like', '%'.$value.'%');
    }

}