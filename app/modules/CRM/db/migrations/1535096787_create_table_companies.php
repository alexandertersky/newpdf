<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_companies_1535096787 {
    public function up() {
        Capsule::schema()->create('companies', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('name')->nullable();
            $table->text('requisites')->nullable();
            $table->text('representative_header')->nullable();
            $table->text('representative_footer')->nullable();
            $table->integer("type");
            $table->integer('logo_id')->nullable();
        });
    }

    public function down() {
        Capsule::schema()->dropIfExists('companies');
    }
}
