<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_records_1535097061 {
    public function up() {
        Capsule::schema()->create('records', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->string('record');
        });
    }

    public function down() {
        Capsule::schema()->dropIfExists('records');
    }
}
