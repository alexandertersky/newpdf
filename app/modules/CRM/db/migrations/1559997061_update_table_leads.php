<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class update_table_leads_1559997061 {
    public function up() {
        Capsule::schema()->table('leads', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->integer('crm_group_id')->unsigned()->nullable();
            $table->integer('position')->unsigned()->nullable();

            $table->foreign('crm_group_id')
                ->references('id')
                ->on('crm_groups')
                ->onDelete('set null');
        });
    }

    public function down() {
    }
}
