<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_recorded_1535097485 {
    public function up() {
        Capsule::schema()->create('recorded', function($table) {
            $table->increments('id');
            $table->integer('record_id');
            $table->integer('recorded_id');
            $table->string('recorded_type');
        });
    }

    public function down() {
        Capsule::schema()->dropIfExists('recorded');
    }
}
