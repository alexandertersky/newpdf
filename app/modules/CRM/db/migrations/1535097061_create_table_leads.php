<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_leads_1535097061 {
    public function up() {
        Capsule::schema()->create('leads', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->integer('callcenter_id')->unsigned();
            $table->string('name');
            $table->text('comment')->nullable();
            $table->string('email')->nullable();
            $table->string('company')->nullable();
            $table->text('source')->nullable();
            $table->date('birthday')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('skype')->nullable();
            $table->string('twitter')->nullable();
            $table->string('vk')->nullable();
            $table->dateTime('created_at');
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });
    }

        public function down() {
        Capsule::schema()->dropIfExists('leads');
    }
}
