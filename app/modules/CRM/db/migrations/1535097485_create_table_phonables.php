<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_phonables_1535097485 {
    public function up() {
        Capsule::schema()->create('phonables', function($table) {
            $table->increments('id');
            $table->integer('phone_id');
            $table->integer('phonable_id');
            $table->string('phonable_type');
        });
    }

    public function down() {
        Capsule::schema()->dropIfExists('phonables');
    }
}
