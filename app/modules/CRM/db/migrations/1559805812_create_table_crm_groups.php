<?php

use Illuminate\Database\Capsule\Manager as Capsule;


class create_table_crm_groups_1559805812 {
    public function up() {
        Capsule::schema()->create('crm_groups', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->integer('position')->unsigned()->nullable();
            $table->string('scope');
        });
    }

    public function down() {
        Capsule::schema()->dropIfExists('crm_groups');
    }
}
