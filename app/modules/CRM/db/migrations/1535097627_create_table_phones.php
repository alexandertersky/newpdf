<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_phones_1535097627 {
    public function up() {
        Capsule::schema()->create('phones', function($table) {
            $table->increments('id');
            $table->string('phone');
        });
    }

    public function down() {
        Capsule::schema()->dropIfExists('phones');
    }
}
