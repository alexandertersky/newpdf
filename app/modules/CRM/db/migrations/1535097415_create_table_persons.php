<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_persons_1535097415 {
    public function up() {
        Capsule::schema()->create('persons', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->nullable();
            $table->string('name')->nullable();
            $table->text('comment')->nullable();
            $table->string('email')->nullable();
            $table->text('source')->nullable();
            $table->date('birthday')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });
    }

    public function down() {
        Capsule::schema()->dropIfExists('persons');
    }
}
