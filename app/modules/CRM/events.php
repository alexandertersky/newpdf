<?php 

use App\Helpers\Event;

Event::addListener('render_navigation', function ($event, &$items) {
    $items[] = (array)json_decode('{"title":"Лиды","icon":"icon-phone","path":"lead.showAdminLeadList"}');
    $items[] = (array)json_decode('{"title":"Компании","icon":"icon-magazine","path":"company.showAdminCompanyList"}');
    $items[] = (array)json_decode('{"title":"Клиенты","icon":"icon-person","path":"person.showAdminPersonList"}');
});