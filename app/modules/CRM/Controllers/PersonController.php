<?php

namespace App\Modules\CRM\Controllers;

use App\Helpers\Mailer;
use App\Controllers\Controller;
use App\Modules\CRM\Models\Company;
use App\Modules\CRM\Models\Lead;
use App\Modules\CRM\Models\Person;
use App\Modules\CRM\Models\Phone;
use App\Models\Tag;

use Slim\Http\Request;
use Slim\Http\Response;
use Carbon\Carbon;

class PersonController extends Controller
{
    public function showAdminPersonList($request, $response, $args)
    {
        $data = $request->getParams();

        $person = Person::with('phones', 'tags', 'company');
        if (isset($data['filter']) && $data['filter']){
            $person = $person->filter($data['filter']);
            $this->twig_vars['filter'] = $data['filter'];
        }

        $person = $person->paginate(50)->toArray();
        if (isset($data['company'])) {
            $person = $person->where('company_id', $data['company']);
        }
        $this->twig_vars['companies'] = Company::all()->toArray();
        $this->twig_vars['tags'] = Tag::all()->toArray();
        $this->twig_vars['data'] = $data;
        $this->twig_vars['persons'] = $person;
        $this->render('admin/persons/persons-list.twig');
    }

    public function showAdminPersonEdit($request, $response, $args)
    {
        $this->twig_vars["companies"] = Company::all()->toJson();
        $this->twig_vars['person'] = Person::with('tags', 'phones', 'company')->find($args['id'])->toArray();
        $this->twig_vars["tags"] = Tag::all()->toJson();
        $this->render('admin/persons/person-form.twig');
    }

    public function showAdminPersonAdd($request, $response, $args)
    {
        $this->twig_vars['companies'] = Company::all()->toJson();
        $this->twig_vars['tags'] = Tag::all()->toJson();
        $this->render('admin/persons/person-form.twig');
    }

    public function createPerson($request, $response, $args)
    {
        $data = $request->getParams();
        $person = Person::create($data['person']);
        foreach ($data['phone'] as $phone) {
            $newPhone = Phone::firstOrCreate(['phone' => str_replace(['+','(', ')','-', ' ', '—'], '', $phone)]);
            $person->phones()->attach($newPhone->id);
        }
        $person->tags()->attach($data["tags"]);
        return $response->withRedirect($this->ci->router->pathFor('person.showAdminPersonList'));
    }

    public function createPersonFromLead($request, Response $response, $args)
    {
        $lead = Lead::find($args["id"]);
        $tags = $lead->tags;
        $phones = $lead->phones;
        $lead = $lead->toArray();
        unset($lead["phones"], $lead["tags"], $lead["id"], $lead["company"], $lead["created_at"], $lead["updated_at"], $lead["deleted_at"]);
        $person = Person::create($lead);
        foreach ($phones as $phone) {
            $person->phones()->attach($phone->id);
        }
        foreach ($tags as $tag) {
            $person->tags()->attach($tag->id);
        }
        return $response->withRedirect($this->ci->router->pathFor("person.showAdminPersonEdit", ["id" => $person->id]));
    }

    public function updatePerson($request, $response, $args)
    {
        $data = $request->getParams();
        $person = Person::find($args['id']);
        $person->update($data['person']);
        $person->tags()->detach();
        $person->tags()->attach($data["tags"]);

        $phones_id = [];
        foreach ($data["phone"] as $phone){
            array_push($phones_id, Phone::firstOrCreate(['phone' => str_replace(['+','(', ')','-', ' ', '—'], '', $phone)])->id);
        }
        $person->phones()->sync($phones_id);
        return $response->withRedirect($this->ci->router->pathFor('person.showAdminPersonList'));
    }

    public function deletePerson($request, $response, $args)
    {
        $person = Person::find($args['id']);
        $person->tags()->detach();
        $person->phones()->detach();
        $person->destroy($args['id']);
        return $response->withRedirect($this->ci->router->pathFor('person.showAdminPersonList'));
    }

    public function findBirthday()
    {
        $now = Carbon::now();
        $persons = Person::with('phones')->whereDay('birthday', $now->format('d'))->whereMonth('birthday', $now->format('m'))->get();
        foreach ($persons as $person) {
            if (strlen($person->email) > 0) {
                echo "email: " . $person->email;
            } else {
                if ($person->phones()->count() > 0) {
                    foreach ($person->phones as $phone) {
                        echo "tel: " . $phone->phone;
                    }
                }
            }
        }
    }

    public function showAdminSendMail()
    {
        $this->view = 'admin/persons/person-send.twig';
        $this->twig_vars['tags'] = Tag::all()->sortBy('title')->toArray();
        $this->twig_vars['persons'] = Person::all()->sortByDesc('created_at')->toArray();
        $this->render();
    }

    public function sendMailByTags(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        $persons = Person::whereNotNull('email')
            ->whereHas('tags', function ($query) use ($data) {
                $query->whereIn('tags.id', $data['tags']);
            })
            ->orWhereIn('id', $data['persons'])
            ->get(['email']);
        $mailer = new Mailer();
        $addresses = [];
        foreach ($persons as $person) {
            $addresses[] = $person['email'];
        }
//        ddd($addresses);
        $mailer->setAddresses($addresses)->send($data['header'], $data['body']);
        return $response->withRedirect('/admin/persons');
    }

}