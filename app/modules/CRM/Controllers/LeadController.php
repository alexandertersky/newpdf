<?php

namespace App\Modules\CRM\Controllers;

use App\Modules\CRM\Models\Company;
use App\Modules\CRM\Models\Lead;
use App\Modules\CRM\Models\CRMGroup;
use App\Modules\CRM\Models\Phone;
use App\Modules\CRM\Models\Record;
use App\Models\Tag;
use App\Controllers\Controller;

use Slim\Http\Request;

class LeadController extends Controller
{
    public function showAdminLeadList(Request $request, $response, $args)
    {
        $data = $request->getParams();
        $leads = Lead::with('phones', 'tags');

        $groups = CRMGroup::where('scope', 'leads')->get();

        if (isset($data['filter']) && $data['filter']){
            $leads = $leads->filter($data['filter']);
            $this->twig_vars['filter'] = $data['filter'];
        }

        $leads = $leads->get();
        $this->twig_vars['leads'] = $leads;
        $this->twig_vars['groups'] = $groups->toArray();
        $this->twig_vars['tags'] = Tag::all()->toArray();
        $this->twig_vars['companies'] = Company::all()->toArray();

        $this->render('admin/leads/leads-list.twig');
    }

    public function showAdminLeadEdit($request, $response, $args)
    {
        $this->twig_vars['lead'] = Lead::with('phones', 'records', 'tags')->find($args['id'])->toArray();
        $this->twig_vars['tags'] = Tag::all()->toJson();
        $this->render('admin/leads/lead-form.twig');
    }

    public function showAdminLeadAdd($request, $response, $args)
    {
        $this->twig_vars['tags'] = Tag::all()->toJson();
        $this->render('admin/leads/lead-form.twig');
    }

    public function createLead($request, $response, $args)
    {
        $data = $request->getParams();

        $lead = Lead::create($data['lead']);

        if(isset($data['tags'])) $lead->tags()->attach($data['tags']);

        foreach ($data["phone"] as $phone) {
            $newPhone = Phone::firstOrCreate(['phone' => str_replace(['+','(', ')','-', ' ', '—'], '', $phone)]);
            $lead->phones()->attach($newPhone->id);
        }
        return $response->withRedirect($this->ci->router->pathFor('lead.showAdminLeadList'));
    }

    public function updateLead($request, $response, $args)
    {
        $data = $request->getParams();
        $lead = Lead::find($args['id']);

        if (isset($data['lead']['crm_group_id']) && $data['lead']['crm_group_id'] == 0) {
            $data['lead']['crm_group_id'] = null;
        }

        $lead->update($data['lead']);

        if (isset($data['tags'])) {
            $lead->tags()->sync($data["tags"]);
        }

        if (isset($data['phone'])) {
            $phones_id = [];

            foreach ($data["phone"] as $phone){
                $phones_id[] = Phone::firstOrCreate(['phone' => str_replace(['+','(', ')','-', ' ', '—'], '', $phone)])->id;
            }

            $lead->phones()->sync($phones_id);
        }

        return $response->withRedirect($this->ci->router->pathFor('lead.showAdminLeadList'));
    }

    public function importLead($request, $response, $args)
    {
        if (!isset($_FILES['file']))
            return $response->withRedirect($this->ci->router->pathFor('lead.showAdminLeadList'));

        $file = $_FILES['file'];

        if ($file['type'] !== 'application/json') {
            die('Не верный тип файла<br><a href="' . $this->ci->router->pathFor('lead.showAdminLeadList') . '">Вернуться назад</a>');
        }

        $leads = json_decode(file_get_contents($file['tmp_name']), true);

        foreach ($leads as $lead) {
            $lead['name'] = $lead['title'];
            $phones = $lead['phones'];
            $records = [];

            unset($lead['title']);
            unset($lead['phones']);

            if (isset($lead['records'])) {
                $records = $lead['records'];
            }

            unset($lead['records']);

            $l = Lead::updateOrCreate(['callcenter_id' => $lead['callcenter_id']], $lead);

            $phones_id = [];
            foreach ($phones as $phone){
                $phones_id[] = Phone::firstOrCreate(compact('phone'))->id;
            }

            $l->phones()->sync($phones_id);

            if(count($records) > 0) {

                $records_id = [];
                foreach ($records as $record) {
                    $records_id[] = Record::firstOrCreate(compact('record'))->id;
                }

                $l->records()->sync($records_id);
            }
        }

        return $response->withRedirect($this->ci->router->pathFor('lead.showAdminLeadList'));
    }

    public function deleteLead($request, $response, $args)
    {
        $lead = Lead::find($args["id"]);
        $lead->tags()->detach();
        $lead->phones()->detach();
        $lead->destroy($args['id']);
        return $response->withRedirect($this->ci->router->pathFor('lead.showAdminLeadList'));
    }

    public function createLeadGroup($request, $response, $args)
    {

        $group = CRMGroup::create([
            'title' => 'Новая группа',
            'scope' => 'leads',
            'position' => 0
        ]);

        return $group->toJson();
    }

    public function updateLeadGroup($request, $response, $args)
    {
        CRMGroup::where('id', $args['id'])->update($request->getParams());
    }

    public function deleteLeadGroup($request, $response, $args)
    {
        CRMGroup::where('id', $args['id'])->delete();
    }

}