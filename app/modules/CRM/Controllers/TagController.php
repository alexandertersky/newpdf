<?php

namespace App\Modules\CRM\Controllers;

use App\Modules\CRM\Models\Tag;
use App\Controllers\Controller;

use Slim\Http\Response;

class TagController extends Controller
{

    public function createTag($request,Response $response, $args)
    {
        $data = $request->getParams();
        $tag = Tag::create($data);
        return $response->write($tag->id);
    }

    public function updateTag($request, $response, $args)
    {
        $data = $request->getParams();
        Tag::find($args['id'])->update($data['tag']);
        return $response->withRedirect($this->ci->router->pathFor('tag.showAdminTagList'));
    }

    public function deleteTag($request, $response, $args)
    {
        Tag::destroy($args['id']);
        return $response->withRedirect($this->ci->router->pathFor('tag.showAdminTagList'));
    }

}