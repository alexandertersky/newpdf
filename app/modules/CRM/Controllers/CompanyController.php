<?php

namespace App\Modules\CRM\Controllers;

use App\Modules\CRM\Models\Company;
use App\Models\Tag;

use App\Controllers\Controller;

class CompanyController extends Controller
{
    public function showAdminCompanyList($request, $response, $args)
    {
        $data = $request->getParams();
        $companies = Company::with('logo');

        if (isset($data['filter']) && $data['filter']){
            $this->twig_vars['filter'] = $data['filter'];
            $companies = $companies->filter($data['filter']);
        }

        $this->twig_vars['companies'] = $companies->paginate(50)->toArray();
        $this->render('admin/companys/companys-list.twig');
    }

    public function showAdminCompanyEdit($request, $response, $args)
    {
        $this->twig_vars['company'] = Company::with('tags', 'logo')->find($args['id'])->toArray();
        $this->twig_vars["tags"] = Tag::all()->toJson();
        $this->render('admin/companys/company-form.twig');
    }

    public function showAdminCompanyAdd($request, $response, $args)
    {
        $this->twig_vars["tags"] = Tag::all()->toJson();
        $this->render('admin/companys/company-form.twig');
    }

    public function showAdminRequisitesEdit($request, $response, $args)
    {
        $this->twig_vars['company'] = Company::find($args['id'])->toArray();
        $this->render('admin/companys/company-requisites.twig');
    }

    public function createCompany($request, $response, $args)
    {
        $data = $request->getParams();
        $file = $this->_uploadFiles('file', '/files')[0];
        $data['company']['logo_id'] = $file['id'];
        $company = Company::create($data['company']);
        $company->tags()->attach($data['tags']);
        return $response->withRedirect($this->ci->router->pathFor('company.showAdminCompanyList'));
    }

    public function updateCompany($request, $response, $args)
    {
        $data = $request->getParams();
        if ($_FILES['file']['size'] > 0) {
            $file = $this->_uploadFiles('file', '/files')[0];
            $data['company']['logo_id'] = $file['id'];
        }
        $company = Company::find($args['id']);
        $company->update($data['company']);
        $company->tags()->sync($data['tags']);
        return $response->withRedirect($this->ci->router->pathFor('company.showAdminCompanyList'));
    }

    public function deleteLogo($request, $response, $args)
    {
        $id = $args['id'];
        $company = Company::find($id);
        if ($company->logo) {
            $company->logo->delete();
        }
        return $response->withRedirect($this->ci->router->pathFor('company.showAdminCompanyEdit', ['id' => $id]));
    }

    public function deleteCompany($request, $response, $args)
    {
        $company = Company::find($args['id']);
        $company->tags()->detach();
        $company->delete();
        return $response->withRedirect($this->ci->router->pathFor('company.showAdminCompanyList'));
    }

}