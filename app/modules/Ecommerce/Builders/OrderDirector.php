<?php


namespace App\Modules\Ecommerce\Builders;

use App\Models\User;

class OrderDirector
{
    private $builder;

    public function __construct()
    {
        $this->builder = new OrderBuilder();
    }

    public function buildPublicOrder(string $phone, array $products)
    {
        $this->builder->setSimpleUser($phone);
        $this->builder->setPublicCart($products);
        $this->builder->setProducts();
        $this->builder->checkCart();
        $this->builder->setPurchases();
        return $this->builder->create();
    }

    public function buildAdminOrder(string $phone, array $products)
    {
        $this->builder->setSimpleUser($phone);
        $this->builder->setAdminCart($products);
        $this->builder->setProducts();
        $this->builder->checkCart();
        $this->builder->setPurchases();
        return $this->builder->create();
    }
}