<?php

namespace App\Modules\Ecommerce\Builders;

use App\Helpers\Config;
use App\Models\User;
use App\Modules\Ecommerce\Models\Order;
use App\Modules\Ecommerce\Models\Product;
use App\Modules\Ecommerce\Models\Purchase;

class OrderBuilder
{
    private $order;
    private $container;
    private $user;
    public $cart;
    private $products;
    private $cost;

    public function __construct()
    {
        $this->order = new Order();
        $this->order->status = Order::CREATING_STATUS;
        $this->container = Config::getInstance()['container'];
    }

    /**
     * Получаем или регистрируем пользователя
     * @param string $phone
     */
    public function setSimpleUser(string $phone)
    {
        $user = User::where('phone', $phone)->first();

        // Если пользователь не зарегистрирован, создаём его по номеру телефона
        if (!$user) {
            $user = User::create(['phone' => $phone]);

            $role = $this->container->sentinel->findRoleBySlug('buyer');

            if (!$role) {
                ddd('SET ROLES!');
            }

            $role->users()->attach($user);
        }

        $this->user = $user;
    }

    public function setBulkUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Получаем корзину, созданную с сайта
     */
    public function setPublicCart($products)
    {
        $cart = [];
        foreach ($products as $product){
            $cart[$product['id']] = [
                'title' => $product['product']['title'],
                'id' => $product['id'],
                'count' => $product['count'],
                'in_storage' => $product['product']['amount'],
                'price' => $product['product']['price']
            ];
        }
        $this->cart = $cart;
    }

    /**
     * Получаем список товаров с обновленными данными
     */
    public function setProducts()
    {
        $products_id = array_keys($this->cart);
        $this->products = Product::find($products_id);
//        $this->products = Product::updateProductInfo($products);
        $this->products->each->setDiscountedPrice($this->user);
    }

    /**
     * Сверяем количество товаров в корзине с остатками на складе
     * За одно считаем стоимость
     *  Т.к. цену товара мы получаем уже с учётом скидки, здесь же учитывается скидка
     */
    public function checkCart()
    {
        foreach ($this->products as $product) {
            if ($product->amount < $this->cart[$product->id]['count']) {
                throw new \Exception('Недостаточно товаров на складе');
            }
            $this->cost += $product->discounted_price * $this->cart[$product->id]['count'];
        }
    }

    public function create()
    {
        $this->order->status = Order::CREATED_STATUS;
        $this->order->total_cost = $this->cost;
        $this->order->user_id = $this->user->id;
        $this->order->save();
        $this->order->products()->sync($this->getProductsToSync());
        return $this->order;
    }

    private function getProductsToSync()
    {
        $arr = [];
        foreach ($this->cart as $id => $c) {
            $arr[$id] = ['amount' => $c['count'], 'price' => $c['price']];
        }
        return $arr;
    }

    public function setPurchases()
    {
        // собираем массив с id продуктов
        $keys = array_keys($this->cart);
        rsort($keys);
        $pairs = [];

        foreach ($keys as $i => $key) {
            for ($c = 0; $c < count($keys) - 1 - $i; $c++) {
                $pair = [];
                $pair['first_id'] = $key;
                $pair['second_id'] = $keys[$c + $i + 1];
                $pairs[] = $pair;
            }
        }


        foreach ($pairs as $pair) {
            $db_pair = Purchase::firstOrNew($pair);
            if ($db_pair->exists) {
                $db_pair->increment('count');
            } else {
                $db_pair->count = 1;
                $db_pair->save();
            }
        }
    }

    public function setAdminCart(array $products)
    {
        $cart = [];
        foreach ($products as $product){
            $cart[$product['id']] = [
                'title' => $product['title'],
                'id' => $product['id'],
                'count' => $product['pivot']['amount'],
                'in_storage' => $product['amount'],
                'price' => $product['discounted_price']
            ];
        }
        $this->cart = $cart;
    }
}