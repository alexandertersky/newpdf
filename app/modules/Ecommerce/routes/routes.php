<?php

use App\Middleware\Registred;

use App\Modules\Ecommerce\Controllers\ProductController;
use App\Modules\Ecommerce\Controllers\CategoryController;
use App\Modules\Ecommerce\Controllers\TagController;
use App\Modules\Ecommerce\Controllers\OrderController;
use App\Modules\Ecommerce\Controllers\UserController;
use App\Modules\Ecommerce\Controllers\CartController;

$c = $app->getContainer();

$app->group('/public', function () use ($app, $c) {

    $app->group('/ecommerce', function () use ($app, $c) {

        $app->group('/cart', function () use ($app, $c) {
            $app->get('', CartController::class.':showPublicCartTest')->setName('public.ecommerce.cart.test');
        });

        $app->get('/order/create', OrderController::class.':createPublicOrder')->setName('public.ecommerce.order.create');
    });

});


$app->group('/admin', function () use ($app, $c) {

    $app->group('/ecommerce', function () use ($app, $c) {

        $app->group('/products', function () use ($app, $c) {
            // Список товаров
            $app->get('', ProductController::class . ':showAdminProductList')->setName('admin.ecommerce.products.list');
            // Создание товара
            $app->post('', ProductController::class . ':createProduct')->setName('admin.ecommerce.products.create');
            // Форма создания товара
            $app->get('/add', ProductController::class . ':showAdminProductAdd')->setName('admin.ecommerce.products.add');
            // Поиск товаров по названию
            $app->get('/get-by-title', ProductController::class . ':getProductsByTitle')->setName('admin.ecommerce.products.get-by-title');
            // Изменение товара
            $app->post('/{id}', ProductController::class . ':updateProduct')->setName('admin.ecommerce.products.update');
            // Форма изменения товара
            $app->get('/{id}', ProductController::class . ':showAdminProductEdit')->setName('admin.ecommerce.products.edit');
            // Удаление товара
            $app->get('/{id}/delete', ProductController::class . ':deleteProduct')->setName('admin.ecommerce.products.delete');
        });

        $app->group('/categories', function () use ($app) {
            $app->get('', CategoryController::class . ':showAdminCategoriesList')->setName('admin.ecommerce.categories.list');
            $app->get('/add', CategoryController::class . ':showAdminCategoriesAdd')->setName('admin.ecommerce.categories.add');
            $app->post('', CategoryController::class . ':createCategory')->setName('admin.ecommerce.categories.create');
            $app->get('/{id}', CategoryController::class . ':showAdminCategoriesEdit')->setName('admin.ecommerce.categories.edit');
            $app->post('/{id}', CategoryController::class . ':updateCategory')->setName('admin.ecommerce.categories.update');
        });

        $app->group('/tags', function () use ($app) {
            // Список тэгов
            $app->get('', TagController::class . ':showAdminTagsList')->setName('admin.ecommerce.tags.list');
            // Форма изменения тэга
            $app->get('/{id}', TagController::class . ':showAdminTagsEdit')->setName('admin.ecommerce.tags.edit');
            // Изменение тэга
            $app->post('/{id}', TagController::class . ':updateTag')->setName('admin.ecommerce.tags.update');
        });

        $app->group('/orders', function () use ($app) {
            // Список заказов
            $app->get('', OrderController::class . ':showAdminOrdersList')->setName('admin.ecommerce.orders.list');
            // Форма создания заказа
            $app->get('/add', OrderController::class . ':showAdminOrderAdd')->setName('admin.ecommerce.orders.add');
            // Создание заказа
            $app->post('', OrderController::class . ':createAdminOrder')->setName('admin.ecommerce.orders.create');
            // Форма изменения заказа
            $app->get('/{id}', OrderController::class . ':showAdminOrderEdit')->setName('admin.ecommerce.orders.edit');
            // Обновление заказа
            $app->post('/{id}', OrderController::class . ':updateOrder')->setName('admin.ecommerce.orders.update');
        });

        $app->group('/users', function () use ($app) {
            $app->get('/get-discount', UserController::class . ':getDiscountByPhone')->setName('admin.ecommerce.user.get-discount');
        });

    });

})->add(new Registred($c));
