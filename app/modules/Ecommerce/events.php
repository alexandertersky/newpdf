<?php

use App\Helpers\Event;
use Illuminate\Database\Eloquent\Relations\Relation;

use App\Modules\Ecommerce\Models\Order;

Event::addListener('render_navigation', function ($event, &$items) {
    $items[] = [
        'title' => 'Товары',
        'icon' => 'icon-cart5',
        'path' => 'admin.ecommerce.products.list'
    ];

    $items[] = [
        'title' => 'Категории',
        'icon' => 'icon-certificate',
        'path' => 'admin.ecommerce.categories.list'
    ];

    $items[] = [
        'title' => 'Тэги',
        'icon' => 'icon-price-tag2',
        'path' => 'admin.ecommerce.tags.list'
    ];

    $items[] = [
        'title' => 'Заказы',
        'icon' => 'icon-clippy',
        'path' => 'admin.ecommerce.orders.list'
    ];

});

Event::addListener('user_relations', function ($event, &$relations) {
    $relations[] = [
        'name' => 'orders',
        'closure' => function ():Relation {
            return $this->hasMany(Order::class, 'user_id', 'id');
        }
    ];
});