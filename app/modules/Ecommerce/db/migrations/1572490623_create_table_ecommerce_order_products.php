<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_ecommerce_order_products_1572490623 {
    public function up() {
        Capsule::schema()->create('ecommerce_order_products', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->integer('order_id');
            $table->integer('product_id');
            $table->integer('amount');
            $table->double('price');
        });
    }
}
