<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_ecommerce_orders_1572489783 {
    public function up() {
        Capsule::schema()->create('ecommerce_orders', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->integer('status');
            $table->float('total_cost')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamp('created_at')->useCurrent();
        });
    }
}
