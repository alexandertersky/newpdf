<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_ecommerce_products_1572247600 {
    public function up() {
        $table_name = 'ecommerce_products';

        $schema = function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->string('article')->nullable();
            $table->string('title')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('alias')->nullable();
            $table->string('barcode')->nullable();
            $table->text('description')->nullable();
            $table->integer('category_id')->nullable();
            $table->double('price')->nullable();
            $table->integer('amount')->nullable();
            $table->boolean('changed_by_site')->default(0);
            $table->integer('unit')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        };


        if (Capsule::schema()->hasTable($table_name)){
            Capsule::schema()->table($table_name, $schema);
        } else {
            Capsule::schema()->create($table_name, $schema);
        }
    }
}
