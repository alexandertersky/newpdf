<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_ecommerce_purshases_1573015257 {
    public function up() {
        Capsule::schema()->create('ecommerce_purchases', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->integer('first_id');
            $table->integer('second_id');
            $table->integer('count');
        });
    }
}
