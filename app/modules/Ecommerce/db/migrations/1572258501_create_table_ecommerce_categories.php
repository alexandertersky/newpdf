<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_ecommerce_categories_1572258501 {
    public function up() {
        Capsule::schema()->create('ecommerce_categories', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('meta_title')->nullable();
            $table->text('description')->nullable();
            $table->string('meta_description')->nullable();
            $table->integer('parent_id')->nullable();
            $table->string('alias')->nullable();
        });
    }
}
