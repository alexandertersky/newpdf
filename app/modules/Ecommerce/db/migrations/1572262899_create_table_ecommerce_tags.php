<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_ecommerce_tags_1572262899 {
    public function up() {
        Capsule::schema()->create('ecommerce_tags', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('alias')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->text('description')->nullable();
        });
    }
}
