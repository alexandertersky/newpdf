<?php

use Illuminate\Database\Capsule\Manager as Capsule;

use App\Models\Role;

class set_buyer_role_1573013887 {
    public function up() {
        Role::firstOrCreate(
            ['slug' => 'buyer'],
            [
                'name' => 'Покупатель',
                'permissions' => '{}'
            ]
        );
    }
}
