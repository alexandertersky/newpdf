<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class update_table_users_1573009254 {
    public function up() {
        Capsule::schema()->table('users', function(\Illuminate\Database\Schema\Blueprint $table) {
            if (!Capsule::schema()->hasColumn('users', 'phone')){
                $table->string('phone')->nullable();
            }
            if (!Capsule::schema()->hasColumn('users', 'discount')){
                $table->double('discount',4,2)->default(0);
            }
        });
    }
}
