class Cart {
    products = [];
    onAdd;
    useCookie;
    amount_field = false;

    constructor({onAdd, onRemove, useCookie, amountField, onChangeCount}) {
        this.useCookie = useCookie === true;
        this.onAdd = onAdd ? onAdd : () => {};
        this.onRemove = onRemove ? onRemove : () => {};
        this.onChangeCount = onChangeCount ? onChangeCount : () => {};
        this.amount_field = amountField;
        this.setProductsFromCookie();
    }

    add(product, count) {
        // Если товара ещё нет в корзине
        if (this.getIndexById(product.id) === false) {

            // Проверяем, хватает ли на "складе" товара
            if (this.checkCount(product, count)) {
                this.products.push({
                    id: product.id,
                    count: +count,
                    product: product
                });
                this.onAdd(product);
                this.setCookie();
                return true;
            }

        }
        return false;
    }

    remove(id) {
        let index = this.getIndexById(id);
        this.onRemove(this.products[index].product);
        this.products.splice(index, 1);
        this.setCookie();
        return this.getProducts();
    }

    getIndexById(id) {
        let index = false;
        this.products.forEach((p, i) => {
            if (+p.id === +id) {
                index = i;
            }
        });
        return index;
    }

    getProducts() {
        return this.products.filter(p => {
            return p != null;
        })/*.map(p => p.product)*/;
    }

    setCookie() {
        if (this.useCookie) {
            document.cookie = 'cart_products=' + encodeURIComponent(JSON.stringify(this.products)) + '; path=/;';
        }
    }

    setProductsFromCookie() {
        if (this.useCookie) {
            var cookies_arr = document.cookie.split(' ');
            var cookies = {};
            cookies_arr.map(c => {
                let a = c.split('=');
                a[1] = a[1].replace(';', '');
                cookies[a[0]] = a[1];
            });
            if (cookies.cart_products) {
                this.products = JSON.parse(decodeURIComponent(cookies.cart_products));
            }
        }
    }

    contains(id) {
        return this.getIndexById(id) !== false;
    }

    checkCount(product, count) {
        if (this.amount_field) {
            if (product.hasOwnProperty(this.amount_field)) {
                return product[this.amount_field] >= count;
            } else {
                console.error(`Undefined field ${this.amount_field}`);
                return false;
            }
        }
        return true;
    }

    setCount(id, count) {
        let item = this.products[this.getIndexById(id)];
        if (this.checkCount(item.product, item.count + count)) {
            item.count += count;
            this.setCookie();
            this.onChangeCount(item.product, item.count);
        }
    }
}