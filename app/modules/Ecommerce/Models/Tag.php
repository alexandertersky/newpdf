<?php


namespace App\Modules\Ecommerce\Models;


use App\Helpers\Alias;
use Illuminate\Database\Eloquent\Model;
use Wtolk\Eloquent\Filter;

class Tag extends Model
{
    use Alias; //Трейт для автоматического создания алиаса
    use Filter;
    public static $alias_settings = [
        'source' => 'title',
        'destination' => 'alias',
        'separator' => '-',
        'maxlength' => 200,
        'on' => 'saving'
    ];

    public $table = 'ecommerce_tags';
    public $primaryKey = 'id';
    public $timestamps = false;
    protected $guarded = [];
}