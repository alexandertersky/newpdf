<?php

namespace App\Modules\Ecommerce\Models;


use App\Models\CustomModel;
use App\Models\User;
use Wtolk\Eloquent\Filter;

class Order extends CustomModel
{
    use Filter;

    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'ecommerce_orders';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];

    const CREATING_STATUS = 0;//Заказ на этапе создания
    const CREATED_STATUS = 1;//Заказ создан

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'ecommerce_order_products')->withPivot('amount', 'price');
    }

    public function createdAtDayFilter($value, $builder)
    {
        return $builder->whereDate('created_at', $value);
    }
}