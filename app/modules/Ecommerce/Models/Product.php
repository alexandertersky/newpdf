<?php


namespace App\Modules\Ecommerce\Models;


use App\Helpers\Alias;
use App\Models\CustomModel;
use App\Models\File;
use App\Models\User;
use Wtolk\Eloquent\Filter;

class Product extends CustomModel
{
    use Alias; //Трейт для автоматического создания алиаса
    use Filter;
    public static $alias_settings = [
        'source' => 'title',
        'destination' => 'alias',
        'separator' => '-',
        'maxlength' => 200,
        'on' => 'saving'
    ];

    public $table = 'ecommerce_products';
    public $primaryKey = 'id';
    protected $guarded = [];

    const UNITS = [
        796 => 'шт', // штука
        704 => 'наб', // набор
        778 => 'уп', // упаковка
        736 => 'рул', // рулон
        166 => 'кг', // килограмм
        55 => 'кв м', // квадратный метр
        6 => 'м' // метр
    ];

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, "taggable");
    }

    public function images()
    {
        return $this->morphMany(File::class, 'fileable');
    }

    public function setDiscountedPrice(User $user = null)
    {
        if ($user){
            $this->setAttribute('discounted_price', round($this->price*(100 - $user->discount)/100, 2));
        } else{
            $this->setAttribute('discounted_price', $this->price);
        }
    }
}