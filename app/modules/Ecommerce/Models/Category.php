<?php


namespace App\Modules\Ecommerce\Models;


use App\Helpers\Alias;
use Illuminate\Database\Eloquent\Model;
use Wtolk\Eloquent\Filter;

class Category extends Model
{
    use Filter;

    use Alias; //Трейт для автоматического создания алиаса
    public static $alias_settings = [
        'source' => 'title',
        'destination' => 'alias',
        'separator' => '-',
        'maxlength' => 200,
        'on' => 'saving'
    ];


    public $table = 'ecommerce_categories';
    public $primaryKey = 'id';
    public $timestamps = false;
    protected $guarded = [];
}