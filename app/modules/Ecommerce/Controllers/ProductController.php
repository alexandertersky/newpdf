<?php


namespace App\Modules\Ecommerce\Controllers;


use App\Controllers\Controller;
use App\Models\User;
use App\Modules\Ecommerce\Models\Category;
use App\Modules\Ecommerce\Models\Product;
use App\Modules\Ecommerce\Models\Tag;
use Illuminate\Database\Eloquent\Collection;

class ProductController extends Controller
{
    public function showAdminProductList($request, $response, $args)
    {
        $filter = $request->getParam('filter');
        $this->twig_vars['filter'] = $filter;
        $this->twig_vars['products'] = Product::filter($filter)->paginate(50);
        $this->render('/admin/products/list.twig');
    }

    public function showAdminProductAdd($request, $response)
    {
        $this->twig_vars['categories'] = Category::get()->toArray();
        $this->twig_vars['units'] = Product::UNITS;
        $this->twig_vars['tags'] = Tag::get()->toArray();
        $this->render('/admin/products/form.twig');
    }

    public function showAdminProductEdit($request, $response, $args)
    {
        $this->twig_vars['categories'] = Category::get()->toArray();
        $this->twig_vars['tags'] = Tag::get()->toArray();
        $product = Product::with(['tags', 'images' => function ($q) {
            $q->orderBy('position');
        }])->find($args['id']);
        $product->setRelation('tags', $product->tags->pluck('id'));
        $this->twig_vars['product'] = $product->toArray();
        $this->twig_vars['units'] = Product::UNITS;
        $this->render('/admin/products/form.twig');
    }

    public function createProduct($request, $response)
    {
        $data = $request->getParams();

        $product = Product::create($data['product']);

        // Обновляем изображения
        $product->checkFiles('images', $data['uploader']);

        // Обновляем тэги
        $tags = new Collection();
        foreach ($data['tags'] as $tag) {
            $tag = Tag::firstOrCreate(['title' => $tag]);
            $tags->add($tag);
        }
        $product->tags()->sync($tags->pluck('id')->toArray());
        return $response->withRedirect($this->ci->router->pathFor('admin.ecommerce.products.list'));
    }

    public function updateProduct($request, $response, $args)
    {
        $data = $request->getParams();

        //Получаем товар
        $product = Product::find($args['id']);
        // Обновляем данные о нём
        $product->update($data['product']);
        // Обновляем изображения
        $product->checkFiles('images', $data['uploader']);

        // Обновляем тэги
        $tags = new Collection();
        foreach ($data['tags'] as $tag) {
            $tag = Tag::firstOrCreate(['title' => $tag]);
            $tags->add($tag);
        }
        $product->tags()->sync($tags->pluck('id')->toArray());
        return $response->withRedirect($this->ci->router->pathFor('admin.ecommerce.products.list'));
    }

    public function deleteProduct($request, $response, $args)
    {
        Product::destroy($args['id']);
        return $response->withRedirect($this->ci->router->pathFor('admin.ecommerce.products.list'));
    }


    public function getProductsByTitle($request, $response)
    {
        $data = $request->getParams();
        $products = Product::orderBy('title');
        $t = $data['title'] !== '';
        $a = $data['article'] !== '';

        if (!$a && !$t){
            return $response->withJson(['status' => false, 'message' => 'Уточните параметры товара']);
        }

        if ($data['title'] !== ''){
            $products->where('title', 'like', '%'.$data['title'].'%');
        }

        if ($data['article'] !== ''){
            $products->where('article', 'like', '%'.$data['article'].'%');
        }

        $count = $products->count();

        if ($count > 20){
            return $response->withJson(['status' => false, 'message' => 'Уточните параметры товара', 'count' => $count]);
        }

        $products = $products->get();

        $user = User::find($data['user_id']);

        $products = $products->map(function ($product) use ($user){
            $product->setDiscountedPrice($user);
            return $product;
        });

        return $response->withJson(['status' => true, 'products' => $products->toArray()]);
    }
}