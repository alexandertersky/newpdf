<?php


namespace App\Modules\Ecommerce\Controllers;


use App\Controllers\Controller;
use App\Modules\Ecommerce\Models\Category;

class CategoryController extends Controller
{
    public function showAdminCategoriesList($request)
    {
        $filter = $request->getParam('filter');
        $this->twig_vars['categories'] = Category::filter($filter)->paginate(10)->toArray();
        $this->twig_vars['filter'] = $filter;
        return $this->render('/admin/categories/list.twig');
    }

    public function showAdminCategoriesAdd()
    {
        return $this->render('/admin/categories/form.twig');
    }

    public function showAdminCategoriesEdit($reqeust, $response, $args)
    {
        $this->twig_vars['category'] = Category::find($args['id'])->toArray();
        return $this->render('/admin/categories/form.twig');
    }

    public function createCategory($request, $response)
    {
        $data = $request->getParam('category');
        Category::create($data);
        return $response->withRedirect($this->ci->router->pathFor('admin.ecommerce.categories.list'));
    }

    public function updateCategory($request, $response, $args)
    {
        $data = $request->getParam('category');
        Category::find($args['id'])->update($data);
        return $response->withRedirect($this->ci->router->pathFor('admin.ecommerce.categories.list'));
    }
}