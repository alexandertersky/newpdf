<?php


namespace App\Modules\Ecommerce\Controllers;


use App\Controllers\Controller;
use App\Models\User;
use Slim\Http\Request;
use Slim\Http\Response;

class UserController extends Controller
{
    public function getDiscountByPhone(Request $request, Response $response)
    {
        $user = User::where('phone', $request->getParam('phone'))->first();
        $discount = 0;
        $user_id = false;
        if ($user){
            $discount = $user->discount;
            $user_id = $user->id;
        }

        return $response->withJson(compact('discount', 'user_id'));
    }
}