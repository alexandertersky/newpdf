<?php


namespace App\Modules\Ecommerce\Controllers;


use App\Controllers\Controller;
use App\Modules\Ecommerce\Models\Tag;

class TagController extends Controller
{
    public function showAdminTagsList()
    {
        $this->twig_vars['tags'] = Tag::paginate(20)->toArray();
        return $this->render('/admin/tags/list.twig');
    }

    public function showAdminTagsEdit($request, $reponse, $args)
    {
        $tag = Tag::find($args['id']);
        $this->twig_vars['tag'] = $tag->toArray();
        return $this->render('/admin/tags/form.twig');
    }

    public function updateTag($request, $response, $args)
    {
        $data = $request->getParam('tag');
        $tag = Tag::find($args['id']);
        $tag->update($data);
        return $response->withRedirect($this->ci->router->pathFor('admin.ecommerce.tags.list'));
    }
}