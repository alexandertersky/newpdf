<?php


namespace App\Modules\Ecommerce\Controllers;


use App\Controllers\Controller;
use App\Helpers\Cookie;
use App\Models\User;
use App\Modules\Ecommerce\Builders\OrderDirector;
use App\Modules\Ecommerce\Models\Order;
use Slim\Http\Request;
use Slim\Http\Response;

class OrderController extends Controller
{
    public function showAdminOrdersList($request)
    {
        $this->twig_vars['orders'] = Order::with('user')->filter($request->getParam('filter'))->paginate(20)->toArray();
        $this->twig_vars['filter'] = $request->getParam('filter');
        return $this->render('/admin/orders/list.twig');
    }

    public function showAdminOrderAdd()
    {
        return $this->render('/admin/orders/form.twig');
    }

    public function createAdminOrder($request, $response)
    {
        $data = $request->getParams();

        $products = json_decode($data['products'], true);
        $phone = str_replace(['-', '+', '(', ')'], '', $data['user']['phone']);

        $director = new OrderDirector();

        try {
            $order = $director->buildAdminOrder($phone, $products);
        } catch (\Exception $e) {
            return $response->withJson(['status' => false, 'message' => $e->getMessage()]);
        }
        return $response->withJson(['status' => true, 'href' => $this->ci->router->pathFor('admin.ecommerce.orders.edit', ['id' => $order->id]).'#success']);
    }

    public function showAdminOrderEdit($request, $response, array $args)
    {
        $order = Order::with('user', 'products')->find($args['id']);
        $user = $order->user;
        $order->products->map(function ($product) use ($user) {
            $product->setDiscountedPrice($user);
        });
        $this->twig_vars['order'] = $order->toArray();
        return $this->render('/admin/orders/form.twig');
    }

    public function createPublicOrder($request, $response)
    {
        $cart = json_decode($_COOKIE['cart_products'], true);
        $phone = $request->getParam('phone');
        $director = new OrderDirector();
        try {
            $order = $director->buildPublicOrder($phone, $cart);
        } catch (\Exception $e) {
            return $response->withJson(['status' => false, 'message' => $e->getMessage()]);
        }
        Cookie::delete('cart_products');
        ddd(['status' => true, 'message' => 'Заказ #'.$order->id.' успешно создан']);
//        return $response->withJson(['status' => true, 'message' => 'Заказ #'.$order->id.' успешно создан']);
    }
}