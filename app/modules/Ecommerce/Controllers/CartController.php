<?php


namespace App\Modules\Ecommerce\Controllers;


use App\Controllers\Controller;
use App\Modules\Ecommerce\Models\Product;

class CartController extends Controller
{
    public function showPublicCartTest()
    {
        $this->twig_vars['products'] = Product::get();
        return $this->render('/public/cart/test.twig');
    }
}