<?php

use FileUploader\Controllers\UploaderController;

$app->get('/uploader', UploaderController::class.':uploaderPage');
$app->get('/uploader/{user_id}', UploaderController::class.':uploaderExistsPage');

$app->post('/uploader', UploaderController::class.':upload');
$app->post('/uploader/{user_id}', UploaderController::class.':uploadExists');