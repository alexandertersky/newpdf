<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Middleware\Access;
use App\Middleware\Registred;

use App\Controllers\TransactionController;

$c = $app->getContainer();

$app->group('/api', function() use($app, $c) {
    $app->post('/transactions', TransactionController::class.':createTransaction')
    	->setName('transaction.createTransaction')
    	->add(new Access($c, ['transactions_create']));

    $app->post('/transactions/{id}', TransactionController::class.':updateTransaction')
    	->setName('transaction.updateTransaction')
    	->add(new Access($c, ['transactions_update']));

    $app->delete('/transactions/{id}', TransactionController::class.':deleteTransaction')
    	->setName('transaction.deleteTransaction')
    	->add(new Access($c, ['transactions_delete']));

})->add(new Registred($c));