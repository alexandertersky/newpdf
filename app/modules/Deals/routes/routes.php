<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Middleware\Access;
use App\Middleware\Registred;

use App\Modules\Deals\Controllers\TransactionController;

$c = $app->getContainer();

$app->group('/admin', function() use($app, $c) {
	$app->get('/transactions', TransactionController::class . ':showAdminTransactionList')
		->setName('transaction.showAdminTransactionList')
		->add(new Access($c, ['transactions_view']));

    $app->get('/transactions/add', TransactionController::class . ':showAdminTransactionAdd')
    	->setName('transaction.showAdminTransactionAdd')
    	->add(new Access($c, ['transactions_create']));

    $app->get('/transactions/{id}', TransactionController::class . ':showAdminTransactionEdit')
    	->setName('transaction.showAdminTransactionEdit')
    	->add(new Access($c, ['transactions_update']));
    	
})->add(new Registred($c));