<?php

namespace App\Modules\Deals\Controllers;

use App\Modules\CRM\Models\Person;
use App\Modules\Deals\Models\Transaction;

use App\Controllers\Controller;

use Carbon\Carbon;
use Slim\Http\Request;

class TransactionController extends Controller
{
    public function showAdminTransactionList($request, $response, $args)
    {
        $this->twig_vars['transactions'] = Transaction::with("person")->paginate(50);
        $this-> render('admin/transactions/transactions-list.twig');
    }

    public function showAdminTransactionEdit($request, $response, $args)
    {
        $this->twig_vars['transaction'] = Transaction::with('documents')->find($args['id'])->toArray();
        $this->twig_vars["persons"] = Person::all()->toArray();
        $this->render('admin/transactions/transaction-form.twig');
    }

    public function showAdminTransactionAdd(Request $request, $response, $args)
    {
        $this->twig_vars["persons"] = Person::all()->toArray();
        //когда нажали по кнопке "Провести сделку" на странице клиента
        $this->twig_vars["person_select"] = $request->getParam("person");
        $this->render('admin/transactions/transaction-form.twig');
    }

    public function createTransaction($request, $response, $args)
    {
        $data = $request->getParams();
        Transaction::create($data['transaction']);
        return $response->withRedirect($this->ci->router->pathFor('transaction.showAdminTransactionList'));
    }

    public function updateTransaction($request, $response, $args)
    {
        $data = $request->getParams();
        Transaction::find($args['id'])->update($data['transaction']);
        return $response->withRedirect($this->ci->router->pathFor('transaction.showAdminTransactionList'));
    }

    public function deleteTransaction($request, $response, $args)
    {
        $transaction = Transaction::find($args['id']);
        $files = glob($_SERVER['DOCUMENT_ROOT'].'/files/documents/transaction'.$transaction->id.'/*');
        foreach($files as $file){
            if(is_file($file))
                unlink($file);
        }
        rmdir($_SERVER['DOCUMENT_ROOT'].'/files/documents/transaction'.$transaction->id.'/');
        $transaction->documents()->delete();
        $transaction->delete();
        return $response->withRedirect($this->ci->router->pathFor('transaction.showAdminTransactionList'));
    }

}