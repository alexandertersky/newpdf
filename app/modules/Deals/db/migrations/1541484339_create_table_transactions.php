<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_transactions_1541484339 {
    public function up() {
        Capsule::schema()->create('transactions', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->date('date')->nullable();
            $table->text('description')->nullable();
            $table->string('title')->nullable();
            $table->integer('amount')->nullable();
            $table->integer('person_id')->nullable();
            $table->boolean('basic_documents')->default(0);
            $table->datetime('created_at');
            $table->datetime('updated_at')->nullable();
        	$table->datetime('deleted_at')->nullable();
        });
    }

    public function down() {
        Capsule::schema()->dropIfExists('transactions');
    }
}
