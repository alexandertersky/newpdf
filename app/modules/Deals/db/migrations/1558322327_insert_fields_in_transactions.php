<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class insert_fields_in_transactions_1558322327 {
    public function up() {
        Capsule::schema()->table('transactions', function($table) {
            $table->json('fields')->nullable();
        });
    }

    public function down() {
        Capsule::schema()->dropIfExists('transactions');
    }
}