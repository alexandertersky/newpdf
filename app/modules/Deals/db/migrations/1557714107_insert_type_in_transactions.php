<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class insert_type_in_transactions_1557714107 {
    public function up() {
        Capsule::schema()->table('transactions', function($table) {
            $table->tinyInteger('type')->unsigned()->comment('1 -> Хостинг, 
                                                              2 -> Разработка проекта, 
                                                              3 -> Создание сайта, 
                                                              4 -> Продвижение сайта');
        });
    }

    public function down() {
        Capsule::schema()->dropIfExists('transactions');
    }
}