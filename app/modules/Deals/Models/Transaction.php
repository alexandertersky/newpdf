<?php

namespace App\Modules\Deals\Models;

use App\Models\CustomModel;

class Transaction extends CustomModel
{
    public $timestamps = true;
    protected $guarded = [];
    protected $table = 'transactions';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'id' => 'integer',
		'description' => 'text',
		'title' => 'text',
		'amount' => 'integer',
		'person_id' => 'integer',
        'basic_documents' => 'boolean',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
		'deleted_at' => 'datetime',
        'fields' => 'array'
	];

    public function person()
    {
        return $this->belongsTo("App\Modules\CRM\Models\Person");
    }

    public function documents()
    {
        return $this->hasMany("App\Modules\CRM\Models\Document");
    }

    public function stages()
    {
        return $this->hasMany(Stage::class);
    }

    /**
     * @param string $filename
     * @return Document
     */
    public function getDocument(string $filename)
    {
        $path = '/files/documents/transaction' . $this->id;

        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $path)) {
            mkdir($_SERVER['DOCUMENT_ROOT'] . $path, 0777, true);
        }

        $document = Document::firstOrCreate([
            'title' => $filename,
            'date' => $this->date,
            'filename' => $filename,
            'basic' => 1,
            'path' => $path . '/' . $filename,
            'transaction_id' => $this->id
        ]);

        return $document;
    }

    public function getCurrency()
    {
        $sum = $this->amount;
        $rub = 'рублей';

        if (substr($sum, -1) == '1' && substr($sum, -2) != '11') {
                $rub = 'рубль';
        }

        else if (substr($sum, -1) >= 2 &&
            substr($sum, -1) <= 4 &&
            !in_array(substr($sum, -2), [11, 12, 13, 14])) {
            $rub = 'рубля';
        }

        return $rub;
    }
}