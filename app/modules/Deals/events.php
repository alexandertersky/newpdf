<?php

use App\Helpers\Event;

Event::addListener('render_navigation', function ($event, &$items) {
    $items[] = [
        "title" => "Сделки",
        "icon" => "icon-coin-dollar",
        "path" => "transaction.showAdminTransactionList"
    ];
});