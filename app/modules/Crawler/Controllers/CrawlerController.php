<?php

namespace App\Modules\Crawler\Controllers;

require_once __DIR__ . '/../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Simplon\Spider\Spider;
use Arachnid\Crawler;
use App\Controllers\Controller;
use App\Models\Domain;
use App\Modules\Crawler\Models\Audith;
use App\Modules\Crawler\Models\Meta;

/**
 * 
 */
class CrawlerController extends Controller
{
	public function start($request, $response, $args)
	{
		header('Content-type: text/html; charset=utf-8');
		ob_implicit_flush(true);
		ob_end_flush();

        if (!$request->getParam('domain') || !$request->getParam('protocol')) {
            return $response->withStatus(404)->withHeader('Location', '/404');
        }

        $domain = $request->getParam('protocol') . $request->getParam('domain');     	
		$host = parse_url($domain)['host'];
		$depth = $request->getParam('depth');
		$linkDepth = $depth ? $depth : 1;

        log('Starting audith for ' . $domain);

		$crawler = new Crawler($domain, $linkDepth);
		$links = $crawler->traverse()->getLinks();

		$links = array_filter($links, function($link) use($host) {
			return $link->getHost() == $host;
		});

		$audith = Audith::create(compact('host'));

		log('Count of links found: ' . count($links));

		foreach($links as $link) {

			$linksText = $link->getMetaInfo('linksText');
			$linksText = $linksText ? trim(strip_tags($linksText)) : null;

			$href = $link->getMetaInfo('href');
			$href = $href ? $href : null;

			if ($link->isVisited()) {	
				$h1 = $link->getMetaInfo('h1Contents');
				$h1 = $h1 ? implode('<br>', $h1) : '';

				$meta = Meta::create([
					'audith_id' => $audith->id,
					'url' => $link->getOriginalUrl(),
					'href' => $href,
					'linkText' => $linksText,
					'statusCode' => $link->getStatusCode(),
					'status' => $link->getStatus(),
					'title' => $link->getMetaInfo('title'),
					'keywords' => $link->getMetaInfo('metaKeywords'),
					'description' => $link->getMetaInfo('metaDescription'),
					'headers' => $h1
				]);

			} else {

				$meta = Spider::fetchParse($link->getOriginalUrl());
				$h1 = null;

				if (isset($meta['headlines'])) {
					$h1 = implode('<br>', $meta['headlines']);					
				}

				$title = isset($meta['title']) ? $meta['title'] : null;
				$description = isset($meta['description']) ? $meta['description'] : null;

                $encoding = mb_detect_encoding($description, 'Windows-1251, ASCII, UTF-8');
                $description = iconv($encoding, "UTF-8//IGNORE", $description);

				$meta = Meta::create([
					'audith_id' => $audith->id,
					'url' => $meta['url'],
					'href' => $href,
					'linkText' => $linksText,
					'statusCode' => $meta['statusCode'],
					'status' => $meta['status'],
					'title' => $title,
					'keywords' => $link->getMetaInfo('metaKeywords'),
					'description' => $description,
					'headers' => $h1
				]);					
			}

			$log = $meta->url . " " . $meta->statusCode;

			switch (substr(strval($meta->statusCode), 0, 1)) {
				case '1':
					log($log, 'white', 'grey');
					break;

				case '2':
					log($log, 'green');
					break;

				case '3':
					log($log, 'yellow');
					break;

				case '4':
				case '5':
					log($log, 'white', 'red');
					break;
				
				default:
					log($log);
					break;
			}
		}

		$this->exportXlsx($audith->load('meta'));
		log('Done!');
	}

	public function showCrawler($request, $response, $args)
	{
        $this->twig_vars['audiths'] = Audith::orderBy('id', 'desc')->get();
        $this->render('/crawler.twig');
	}

	public function showCrawlerMeta($request, $response, $args)
	{
        $this->twig_vars['meta'] = Meta::where('audith_id', $args['id'])->get()->toJson();
        $this->render('/crawler-meta.twig');
	}

	private function exportXlsx(Audith $audith)
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'URL');
		$sheet->setCellValue('B1', 'Ссылка');
		$sheet->setCellValue('C1', 'Текст ссылки');
		$sheet->setCellValue('D1', 'Код ответа');
		$sheet->setCellValue('E1', 'Статус');
		$sheet->setCellValue('F1', 'Заголовок страницы');
		$sheet->setCellValue('G1', 'Ключевые слова');
		$sheet->setCellValue('H1', 'Описание');
		$sheet->setCellValue('I1', 'Заголовки');

		$bgcolor = function(&$sheet, $col, $row, $bgcolor) {
				$sheet->getStyle($col . $row)
					  ->getFill()
					  ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    				  ->getStartColor()
    				  ->setARGB('FF' . $bgcolor);
		};

		foreach ($audith->meta->getIterator() as $i => $meta) {
			$sheet->setCellValue('A'.($i+2), $meta->url);
			$sheet->setCellValue('B'.($i+2), $meta->href);
			$sheet->setCellValue('C'.($i+2), $meta->linkTest);
			$sheet->setCellValue('D'.($i+2), $meta->statusCode);
			$sheet->setCellValue('E'.($i+2), $meta->status);
			$sheet->setCellValue('F'.($i+2), $meta->title);
			$sheet->setCellValue('G'.($i+2), $meta->keywords);
			$sheet->setCellValue('H'.($i+2), $meta->description);
			$sheet->setCellValue('I'.($i+2), $meta->headers);

			if ($meta->statusCode >= 400 && $meta->statusCode < 600) {
				$bgcolor($sheet, 'D', $i+2, 'FF0033');
			}

			if (count(explode('<br>', $meta->headers)) > 1) {
				$bgcolor($sheet, 'I', $i+2, 'FF0033');
			}

			if (!filter_var($meta->href, FILTER_VALIDATE_URL) && substr($meta->href, 0, 1) != '/') {
				$bgcolor($sheet, 'B', $i+2, 'FF0033');
			}
		}

		if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/crawler_report')) {
		    mkdir($_SERVER['DOCUMENT_ROOT'] . '/crawler_report');
        }

		$filename = strtotime($audith->created_at) . '_' . $audith->host . '.xlsx';
		$writer = new Xlsx($spreadsheet);
		$writer->save('crawler_report/' . $filename);
		return $filename;
	}

	public function getReport($request, $response, $args)
	{
		$audith_id = $args['audith_id'];
		$audith = Audith::with('meta')
				->find($audith_id);

		$filename = strtotime($audith->created_at) . '_' . $audith->host . '.xlsx';

		if (file_exists($filename) == false) {
			$filename = $this->exportXlsx($audith);
		}

		$this->download('crawler_report/' . $filename);
	}

	public function getLastReport($request, $response, $args)
	{
		$domain = $args['domain'];
		$audith = Audith::where('host', $domain)->orderBy('created_at', 'desc')->first();
		$file = strtotime($audith->created_at) . '_' . $audith->host . '.xlsx';
		$this->download('crawler_report/' . $file);
	}

	private function download($file)
	{
		if (file_exists($file)) {
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
		    header('Content-Disposition: attachment; filename="'.basename($file).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($file));
		    readfile($file);
		    exit;
		}
	}
}

function log($text, $color = 'white', $bgcolor = 'none')
{
		echo sprintf('<p style="color:%s; background-color:%s;">%s</p>', $color, $bgcolor, $text);
        ob_flush();
        flush();
}

