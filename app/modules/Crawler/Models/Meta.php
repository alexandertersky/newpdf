<?php

namespace App\Modules\Crawler\Models;

use App\Models\CustomModel;

class Meta extends CustomModel
{
	protected $table = 'crawler_meta';
	protected $primaryKey = 'id';
	public $timestamps = false;
	protected $fillable = [
		'audith_id',
		'url',
		'href',
		'linkText',
		'statusCode',
		'status',
		'title',
		'keywords',
		'description',
		'headers'
	];
}