<?php

namespace App\Modules\Crawler\Models;

use App\Models\CustomModel;
use App\Modules\Crawler\Models\Meta;

class Audith extends CustomModel
{
	protected $table = 'crawler_audith';
	protected $primaryKey = 'id';
	public $timestamps = true;
	protected $fillable = ['host'];

	public function getTimestampAttribute()
	{
		return strtotime($this->created_at);
	}

	public function meta()
	{
		return $this->hasMany(Meta::class, 'audith_id', 'id');
	}
}