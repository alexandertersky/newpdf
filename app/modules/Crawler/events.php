<?php

use App\Helpers\Event;

Event::addListener('render_navigation', function ($event, &$items) {

    $items[] = [
        'title' => 'Аудит',
        'icon' => 'icon-task',
        'path' => 'crawler.show'
    ];
});