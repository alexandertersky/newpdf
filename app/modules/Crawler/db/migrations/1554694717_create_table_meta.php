<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_meta_1554694717 {
    public function up() {
        Capsule::schema()->create('crawler_meta', function($table) {
            $table->bigIncrements('id');
            $table->integer('audith_id')->unsigned();
            $table->string('url')->nullable();
            $table->string('href')->nullable();
            $table->string('linkText')->nullable();
            $table->integer('statusCode')->unsigned();
            $table->string('status')->nullable();
            $table->string('title')->nullable();
            $table->text('keywords')->nullable();
            $table->text('description')->nullable();
            $table->string('headers')->nullable();
        });

        
    }

    public function down() {
        Capsule::schema()->dropIfExists('crawler_meta');
    }
}
