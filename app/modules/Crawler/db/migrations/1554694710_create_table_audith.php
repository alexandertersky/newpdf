<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_audith_1554694710 {
    public function up() {
        Capsule::schema()->create('crawler_audith', function($table) {
            $table->bigIncrements('id');
            $table->string('host');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });       
    }

    public function down() {
        Capsule::schema()->dropIfExists('crawler_audith');
    }
}
