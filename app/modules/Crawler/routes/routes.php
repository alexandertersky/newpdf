<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Pavlakis\Middleware\Server\Sapi;
use App\Modules\Crawler\Controllers\CrawlerController;
use App\Middleware\Registred;

require_once '../vendor/autoload.php';

$c = $app->getContainer();

$app->group('/crawler', function() use($app, $c) {
	$app->get('', CrawlerController::class.':showCrawler')->setName('crawler.show');
	$app->get('/meta/{id}', CrawlerController::class.':showCrawlerMeta')->setName('crawler.showMeta');
	$app->post('/start', CrawlerController::class.':start')->setName('crawler.start');
	$app->get('/last_report/{domain}', CrawlerController::class.':getLastReport')->setName('crawler.lastreport');
	$app->get('/report/{audith_id}', CrawlerController::class.':getReport')->setName('crawler.report');
})->add(new Registred($c));
