	Vue.component('search', {
		props: ['keywords', 'index'],
		data: function() {
			return {
				query: null,
				filtered: [],
				color: ['#007bff', '#28a745', '#ffc107', '#dc3545', '#6610f2', '#e83e8c', '#fd7e14', '#17a2b8', '#20c997', '#6f42c1'],
				flag: false
			}
		},
		methods: {
			search: function() {
				if (this.query == null || this.query == '') {
					this.$root.filtered_keywords = this.filtered = this.keywords;
					this.flag = false;
				} else {

					this.filtered = new Keyword(this.keywords)
							.search(this.query)
							.getKeywords();

					this.filtered = this.$root.dereact(this.filtered);

					this.$root.filtered_keywords = new Keyword(this.filtered).mark(this.query, this.color).getKeywords();
					this.flag = true;
				}
			},

			clear: function() {
				this.query = null;
				this.search();
			}
		},
		computed: {
			left: function() {
				return 265 * (this.index % 3);
			},

			top: function() {
				let row = Math.floor(this.index / 3);
				let position = row * 46;

				return position;
			},

			height: function() {
				let row = Math.floor(this.index / 3) + 1;
				return (row * 46) + 'px';
			}
		},
		mounted: function() {
			this.color = this.color[this.index % 10];
		},
		template: `
			<div :style="{'min-height': height}">
				<input :style="{left: left+'px', top: top+'px', 'border-color': color}"
					   type="text" class="form-control search"
					   placeholder="Поиск"
					   v-model="query"
					   @input="search">
				<i title="Очистить" class="icon-cross2" @click="clear"
					:style="{color: color, position: 'absolute', left: left+230+'px', top: top+11+'px'}"></i>
				<search v-if="filtered.length && flag" :keywords="filtered" :index="index + 1"></search>
			</div>
		`
	});

    Vue.component('ungrouped-keywords', {
        props: ['kw'],
        data: function() {
            return {
                showOptions: false
            }
        },
        methods: {
            select: function($event) {
				if ($event.shiftKey || $event.ctrlKey ) {
					this.kw.selected = !this.kw.selected;
				}
            },

            dragKeywordStart: function(kw) {
                this.$root.dragKeywordStart(kw);
            },

            ignore: function() {
                this.kw = new Keyword([this.kw]).select(false).ignore().getKeywords()[0];
            }
        },
        template: `
            <tr :class="{'bg-warning': kw.selected}"
                @click="select($event)"
                draggable="true"
                @dragstart="dragKeywordStart(kw)"
                @dragover.prevent>
                <td><input type="checkbox" v-model="kw.selected"></td>
                <td v-html="kw.words"></td>
                <td v-html="kw.views"></td>
                <td>
                    <a href="#" @click.prevent="ignore">В черный список</a>
                </td>
            </tr>
        `
    });

    Vue.component('grouped-keywords', {
        props: ['keyword'],
        methods: {
            ignore: function() {
                this.keyword.ignore = true;
				this.keyword = new Keyword([this.keyword]).ignore().getKeywords()[0];
            },

            ungroup: function() {
				this.keyword = new Keyword([this.keyword]).group(0).getKeywords()[0];
            }
        },
        template: `
            <tr :class="{'bg-warning': keyword.selected}">
                <td><input type="checkbox" v-model="keyword.selected"></td>
                <td v-html="keyword.words"></td>
                <td v-html="keyword.views"></td>
                <td>
                    <a href="#" @click.prevent="ungroup">Убрать из группы</a> <br>
                    <a href="#" @click.prevent="ignore">В черный список</a>
                </td>
            </tr>
        `
    });

    Vue.component('blacklist', {
        props: ['keyword'],
        methods: {
            restore: function() {
				this.keyword = new Keyword([this.keyword]).ignore(false).getKeywords()[0];
            }
        },
        template: `
            <tr :class="{'bg-warning': keyword.selected}">
                <td><input type="checkbox" v-model="keyword.selected"></td>
                <td v-html="keyword.words"></td>
                <td v-html="keyword.views"></td>
                <td><a href="#" @click.prevent="restore">Восстановить</a></td>
            </tr>
        `
    });

	Vue.component('keyword', {
		props: ['keyword', 'showOptions'],
		methods: {
			dragstart: function() {
				if (this.keyword.selected) {
					this.$root.drag_kw = this.$root.keywords.filter(kw => kw.selected && kw.analytic_group_id);
				} else {
					this.$root.drag_kw = [this.keyword];
				}
			},

			select: function($event) {
				if ($event.shiftKey || $event.ctrlKey ) {
					this.keyword.selected = !this.keyword.selected;
				}
			},
		},
		template: `
			<tr :class="{'bg-warning': keyword.selected}"
				@click="select($event)"
				draggable="true"
				@dragstart="dragstart">
				<td v-html="keyword.words"></td>
				<td v-html="keyword.views"></td>
                <td></td>
			</tr>
		`
	});

	Vue.component('keywords-list', {
		props: ['keywords', 'group_id'],
        methods: {
		    dragover: function() {
		        this.$parent.dragover();
            },
            dragleave: function() {
		        this.$parent.dragleave();
            }
        },
		template: `
			<table class="table table-compact" @dragover.prevent="dragover" @dragleave="dragleave">
				<thead>
					<tr>
						<th>Собранные ключевые слова</th>
						<th>Показов в месяц</th>
                        <th></th>
					</tr>
				</thead>
				<tbody>
					<tr is="keyword"
						v-for="kw in keywords"
						v-if="(kw.analytic_group_id == group_id) && kw.ignore == false"
						:keyword="kw"></tr>
				</tbody>
			</table>
		`
	});

	Vue.component('group-controll', {
		props: ['group'],
		data: function() {
			return {
				oldTitle: this.group.title
			}
		},
		methods: {
			addChildGroup: function	() {
			    new Group().setTitle('Новая подгруппа')
                           .setParentId(this.group.id)
                           .create(group => this.$root.groups.push(group));
			    this.group.expand = true;
			},

			removeGroup: function() {

				if (confirm('Удалить группу ' + this.group.title + '? '
					+ 'Все ключевые слова будут перемещены в категорию нераспределенных')) {
					new Group(this.group).remove(keywords => {this.$root.keywords = keywords});

					this.group.parent_id = null; // Скрываем удаленную группу (Очень костыльный способ)
				}
			},

			rename: function() {
				new Group(this.group).update(() => {
					this.oldTitle = this.group.title;
					this.group.edit = false;
				});
			},

			cancel: function() {
				this.group.title = this.oldTitle;
				this.group.edit = false;
			}
		},
		template: `
			<span>
				<span v-if="group.edit" class="group-icons">
					<i class="icon-checkmark2 text-success"	title="Сохранить" @click="rename()"></i>
					<i class="icon-cross3 text-danger" title="Отменить" @click="cancel()"></i>
				</span>

				<span v-else class="group-icons">
					<i 	class="icon-pencil3 text-primary"
					   	title="Переименовать"
					   	style="font-size: 1.1rem"
						@click="group.edit = true">
					</i>
				</span>

				<a href="#" @click="addChildGroup()">Добавить</a>/
				<a href="#" @click="removeGroup()">Удалить</a>
			</span>
		`
	});

	Vue.component('group', {
		delimiters: ['${', '}'],
		props: ['group', 'groups', 'keywords'],
		methods: {
			dragstart: function (group) {
				this.$root.drag_group = this.group;
			},

			drop: function() {

				// Запрещаем перенос группы саму в себя
				if (this.group === this.$root.drag_group) return false;

				if (this.$root.drag_kw) { // drop keyword in group
					new Keyword(this.$root.drag_kw).select(false).group(this.group.id, () => this.$root.drag_kw = null);
				}
				else if (this.$root.drag_group) { // drop group in group
					this.$root.drag_group.parent_id = this.group.id;
					this.group.expand = true;

					new Group(this.$root.drag_group).update(() => this.$root.drag_group = null);
				}

                this.dragleave();
			},

            dragover: function() {
			    this.$el.style.background = "#00c5ff14";
			    this.$el.style.boxShadow = "0px 0px 7px #00c5ff";
            },

            dragleave: function() {
                this.$el.style.background = "#ffffff";
                this.$el.style.boxShadow = "0px 0px 3px #00000040";
            }
		},
		template: ` <li>
					 <div class="group"
						  draggable="true"
						  @dragstart="dragstart"
						  @drop="drop"
						  @dragover.prevent="dragover"
                          @dragleave="dragleave">
					 	<span class="col">
							<span class="expand" v-if="group.expand" @click="group.expand = false">[-]</span>
							<span class="expand" v-else @click="group.expand = true">[+]</span>

							<input v-if="group.edit" type="text" v-model="group.title">
							<span v-else v-html="group.title"></span>
						</span>

						<group-controll :group="group"></group-controll>
			       	</div>

					<groupsList v-if="group.expand" :groups="groups" :keywords="keywords" :parent_id="group.id"></groupsList>
					<keywords-list v-if="group.expand" :keywords="keywords" :group_id="group.id"></keywords-list>

				   </li>
			       `
	});

	Vue.component('groupsList', {
		props: {
			groups: [Array, Object],
			keywords: [Array, Object],
			parent_id: [Number]
		},
		methods: {
            dragover: function() {
                if (this.$root.drag_group) {
                    this.$el.style.borderColor = "#00c5ff";
                }
            },
            dragleave: function() {
                this.$el.style.borderColor = "#ffffff";
            },
            drop: function() {

                if (this.$root.drag_group) {
                    this.$root.drag_group.parent_id = this.parent_id ? this.parent_id : 0;

                    new Group(this.$root.drag_group).update(() => this.$root.drag_group = null);
                }

                this.dragleave();
            }
		},
		template: `
			<ul class="keyword-groups"
                @drop.self.capture="drop"
                @dragover.prevent.self.capture="dragover"
                @dragleave="dragleave">
				<li is="group" v-for="group in groups"
							   v-if="group.parent_id == parent_id"
							   :key="group.id"
							   :group="group"
							   :groups="groups"
						       :keywords="keywords"></li>
			</ul>
		`
	});