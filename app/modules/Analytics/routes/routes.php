<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Modules\Analytics\Controllers\AnalyticController;
use App\Middleware\Registred;

$c = $app->getContainer();

$app->group('/analytics', function() use($app, $c) {
	$app->get('', AnalyticController::class.':showAnalyticsList')->setName('analytic.list');
	$app->get('/new', AnalyticController::class.':showAnalyticNew')->setName('analytic.new');
	$app->get('/{id}', AnalyticController::class.':showAnalytic')->setName('analytic.show');
})->add(new Registred($c));

$app->group('/api/analytics', function() use ($app, $c) {
	$app->get('/{id}/export/csv', AnalyticController::class.':exportCSV')->setName('analytic.export.csv');
    $app->get('/{id}/export/xlsx', AnalyticController::class.':exportXlsx')->setName('analytic.export.xlsx');
	$app->post('/parse', AnalyticController::class.':parseSheet')->setName('analytic.parse');
	$app->post('/save', AnalyticController::class.':saveAnalytic')->setName('analytic.save');

	$app->post('/{id}/group/create', AnalyticController::class.':createGroup')->setName('analytic.group.create');
	$app->post('/group/update', AnalyticController::class.':updateGroup')->setName('analytic.group.update');
	$app->post('/{id}/group/move', AnalyticController::class.':moveGroup')->setName('analytic.group.move');
	$app->post('/group/remove', AnalyticController::class.':removeGroup')->setName('analytic.group.remove');

	$app->post('/{id}/keyword/move', AnalyticController::class.':moveKeyword')->setName('analytic.keyword.move');
	$app->post('/keyword/ignore/{ignored}', AnalyticController::class.':ignoreKeyword')->setName('analytic.keyword.ignore');
})->add(new Registred($c));