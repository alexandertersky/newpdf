<?php

use App\Helpers\Event;

Event::addListener('render_navigation', function ($event, &$items) {

    $items[] = [
        'title' => 'Аналитика',
        'icon' => 'icon-task',
        'path' => 'analytic.list'
    ];
});