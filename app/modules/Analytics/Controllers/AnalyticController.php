<?php

namespace App\Modules\Analytics\Controllers;

use \ZipArchive;

use App\Modules\Analytics\Models\Analytic;
use App\Modules\Analytics\Models\AnalyticKeyword;
use App\Modules\Analytics\Models\AnalyticGroup;

use App\Controllers\Controller;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * 
 **/
class AnalyticController extends Controller
{
	public function parseSheet($request, $response, $args)
	{
		$params = $request->getParams();

		$file = $_FILES['file'];

		if ($file['type'] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
			return false; // ToDo
		}

		$zip = new ZipArchive();

		if ($zip->open($file['tmp_name']) == false) return false;

		$xml_sheet = $zip->getFromName('xl/worksheets/sheet1.xml');
		$xml_strings = $zip->getFromName('xl/sharedStrings.xml');

		$zip->close();

		$sheet = simplexml_load_string($xml_sheet);
		$strings = simplexml_load_string($xml_strings);


		$getXmlValue = function($xml) use($strings) {

			$type = $xml->attributes()->t;

			return $type == 's' ? $strings->si[intval($xml->v)]->t->__toString() : $xml->v->__toString();
		};

		$keywords = [];

		$first = true;
		foreach ($sheet->sheetData->row as $i => $row) {

			if ($first) { // skip headers
				$first = false;
				continue;
			}

			if ($getXmlValue($row->c[0]) == null) continue;

			$keywords[] = [
				'analytic_id' => $params['analytic_id'],
				'words' => $getXmlValue($row->c[0]),
				'views' => $getXmlValue($row->c[1]),
				'analytic_group_id' => null
			];
		}

		AnalyticKeyword::insert($keywords);
		$keywords = AnalyticKeyword::where('analytic_id', $params['analytic_id'])->get();

		return $keywords->toJson();
	}

	public function exportCSV($request, $response, $args)
	{
		$analytic = Analytic::with(['keywords' => function($q) {
			$q->where('ignore', false)->where('analytic_group_id', null)->orWhere('analytic_group_id', 0);
		}])->with(['groups.keywords' => function($q) {
			$q->where('ignore', false);
		}])->find($args['id']);

		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . $analytic->title . '.csv"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');

		$out = fopen('php://output', 'w');

		foreach ($analytic->groups->getIterator() as $group) {
			
			fputcsv($out, [$group->title]);
			fputcsv($out, ['Ключевые слова', 'Просмотров за месяц']);

			foreach ($group->keywords->getIterator() as $kw) {
				fputcsv($out, [$kw->words, $kw->views]);
			}

			fputcsv($out, []);
		}

		fputcsv($out, ['Нераспределенные']);
		fputcsv($out, ['Ключевые слова', 'Просмотров за месяц']);

		foreach ($analytic->keywords->getIterator() as $kw) {
			fputcsv($out, [$kw->words, $kw->views]);
		}

		fputcsv($out, []);
		fputcsv($out, ['Черный список']);
		fputcsv($out, ['Ключевые слова', 'Просмотров за месяц']);

		$blacklist = AnalyticKeyword::where('analytic_id', $args['id'])->where('ignore', 1)->get();

		foreach ($blacklist->getIterator() as $kw) {
			fputcsv($out, [$kw->words, $kw->views]);
		}

		fclose($out);

		die;
	}

	public function exportXlsx($request, $response, $args)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $row = 1;

        $writeExcel = function($groups) use ($sheet, $row, &$writeExcel) {
            foreach ($groups->getIterator() as $group) {
                $sheet->setCellValue('A' . $row, $group->title);

                foreach ($group->keywords->getIterator() as $keyword) {
                    $row++;
                    $sheet->setCellValue('A' . $row, $keyword->words);
                    $sheet->setCellValue('B' . $row, $keyword->views);
                }

                $row += 2; // Пропускаем одну строку

                /*if ($group->child->count() > 0) {
                    $writeExcel($group->child);
                }*/
            }
        };

        $sheet->setTitle('Распределенные');
        $groupsList = AnalyticGroup::where('analytic_id', $args['id'])->with('child')->get();
        $writeExcel($groupsList);

        // Нераспределенные ключевые слова
        $spreadsheet->createSheet();
        $sheet = $spreadsheet->getSheet(1);
        $sheet->setTitle('Не распределенные');

        $keywords = AnalyticKeyword::where('analytic_group_id', 0)->where('ignore', 0)->get();

        foreach ($keywords->getIterator() as $key => $keyword) {
            $sheet->setCellValue('A' . $key, $keyword->words);
            $sheet->setCellValue('B' . $key, $keyword->views);
        }

        // Черный список
        $spreadsheet->createSheet();
        $sheet = $spreadsheet->getSheet(2);
        $sheet->setTitle('Черный список');

        $keywords = AnalyticKeyword::where('ignore', 1)->get();

        foreach ($keywords->getIterator() as $key => $keyword) {
            $sheet->setCellValue('A' . $key, $keyword->words);
            $sheet->setCellValue('B' . $key, $keyword->views);
        }

        $writer = new Xlsx($spreadsheet);

        $analytic = Analytic::find($args['id']);

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $analytic->title . '.xlsx"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $writer->save('php://output');

        die;
    }

	public function createAnalytic($request, $response, $args)
	{
		$params = $request->getParams();
		$title = $params['title'];

		$analytic = Analytic::create(compact('title'));

		return $response->withRedirect($this->ci->router->pathFor('analytic.show', ['id' => $analytic->id]));
	}

	public function showAnalyticNew($request, $response, $args)
	{
        $this->render('/analytic.twig');
	}

	public function showAnalytic($request, $response, $args)
	{
		$analytic = Analytic::with('groups')->find($args['id']);

		$this->twig_vars['analytic'] = $analytic;
        $this->render('/analytic.twig');
	}

	public function showAnalyticsList($request, $response, $args)
	{
		$analytics = Analytic::orderBy('created_at', 'desc')->get();

		$this->twig_vars['analytics'] = $analytics;
        $this->render('/analytics-list.twig');
	}

	public function saveAnalytic($request, $response, $args)
	{
		$params = $request->getParams();

		if (isset($params['id']) && $params['id']) {
			Analytic::where('id', $params['id'])->update($params);
			$id = $params['id'];
		} else {
			$analytic = Analytic::create($params); 
			$id = $analytic->id;
		}

		return $response->withRedirect($this->ci->router->pathFor('analytic.show', compact('id')));
	}

	public function moveKeyword($request, $response, $args)
	{
		$params = $request->getParams();

        foreach ($params['keywords'] as $kw) {
            AnalyticKeyword::where('id', $kw['id'])->update(['analytic_group_id' => $kw['analytic_group_id']]);
        }
	}

	public function ignoreKeyword($request, $response, $args)
	{
		$params = $request->getParams();

		$ignore = $args['ignored'] == 'true';

    	foreach ($params['keywords'] as $keyword) {
    	    AnalyticKeyword::where('id', $keyword['id'])->update(compact('ignore'));
    	}
	}

	public function createGroup($request, $response, $args)
	{
		$params = $request->getParams();
		$group = AnalyticGroup::create($params);
		$group = AnalyticGroup::find($group->id);

		return $group->toJson();
	}

	public function updateGroup($request, $response, $args)
	{
		$model = new AnalyticGroup;
		$params = $request->getParams();

		$id = $params['id'];

		$params = array_intersect_key($params, array_flip($model->getFillable()));
		$params = array_filter($params);

		AnalyticGroup::where('id', $id)->update($params);
	}

	public function moveGroup($request, $response, $args)
	{
		$params = $request->getParams();

		AnalyticGroup::where('id', $params['id'])->update(['parent_id' => $params['parent_id']]);

		return false;	
	}

	public function removeGroup($request, $response, $args)
    {
        $params = $request->getParams();

        $group = AnalyticGroup::with('child')->find($params['id']);

        // Получаем id дочерних категорий
        $getIds = function($group) use(&$getIds) {

            $ids = [$group->id];

            if ($group->child->count()) {
                foreach ($group->child->getIterator() as $child) {
                    $ids = array_merge($ids, $getIds($child));
                }
            }

            return $ids;
        };

        $ids = $getIds($group);

        // Переносим ключевые слова из групп в список нераспределенных
        AnalyticKeyword::whereIn('analytic_group_id', $ids)->update(['analytic_group_id' => 0]);

        // Удаляем группы
        AnalyticGroup::destroy($ids);

        return AnalyticKeyword::where('analytic_id', $params['analytic_id'])->get()->toJson();
    }
}

