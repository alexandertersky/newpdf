<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_analytic_keywords_1556504239 {
    public function up() {
        Capsule::schema()->create('analytic_keywords', function($table) {
            $table->bigIncrements('id');
            $table->integer('analytic_id')->unsigned();
            $table->integer('analytic_group_id')->unsigned()->default(null);
            $table->string('words');
            $table->integer('views')->unsigned()->default(0);
            $table->boolean('ignore')->default(0);
        });

        
    }

    public function down() {
        Capsule::schema()->dropIfExists('analytic_keywords');
    }
}
