<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_analytics_1556504232 {
    public function up() {
        Capsule::schema()->create('analytics', function($table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->timestamps();
        });       
    }

    public function down() {

        Capsule::schema()->dropIfExists('analytics');
    }
}
