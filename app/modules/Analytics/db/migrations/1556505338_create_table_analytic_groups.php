<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_analytic_groups_1556505338 {
    public function up() {
        Capsule::schema()->create('analytic_groups', function($table) {
            $table->bigIncrements('id');
            $table->integer('parent_id')->unsigned()->default(0);
            $table->integer('analytic_id')->unsigned();
            $table->string('title');
        });

        
    }

    public function down() {
        Capsule::schema()->dropIfExists('analytic_groups');
    }
}
