<?php

namespace App\Modules\Analytics\Models;

use App\Modules\Analytics\Models\AnalyticKeyword;
use App\Models\CustomModel;

class AnalyticGroup extends CustomModel
{
	public $timestamps = false;
    protected $table = 'analytic_groups';
    protected $appends = ['expand', 'edit'];
    protected $fillable = [
    	'title',
    	'analytic_id',
    	'parent_id'
    ];

    public function keywords()
    {
    	return $this->hasMany(AnalyticKeyword::class, 'analytic_group_id')->orderBy('views', 'desc');
    }

    public function child()
    {
    	return $this->hasMany(AnalyticGroup::class, 'parent_id', 'id')->with('child');
    }

    public function getExpandAttribute()
    {
    	return $this->attributes['expand'] = false;
    }

    public function getEditAttribute()
    {
    	return $this->attributes['edit'] = false;
    }
}