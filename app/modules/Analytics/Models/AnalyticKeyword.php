<?php

namespace App\Modules\Analytics\Models;

use App\Models\CustomModel;

class AnalyticKeyword extends CustomModel
{
    public $timestamps = false;
    protected $table = 'analytic_keywords';
    protected $appends = ['selected'];
    protected $fillable = ['id', 'ignore'];
    protected $casts = [
        'ignore' => 'boolean'
    ];

    public function getSelectedAttribute()
    {
    	return $this->attributes['selected'] = false;
    }
}