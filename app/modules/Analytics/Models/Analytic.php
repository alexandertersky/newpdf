<?php

namespace App\Modules\Analytics\Models;

use App\Modules\Analytics\Models\AnalyticKeyword;
use App\Modules\Analytics\Models\AnalyticGroup;
use App\Models\CustomModel;

class Analytic extends CustomModel
{
    public $timestamps = true;
    protected $guarded = [];
    protected $table = 'analytics';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'id' => 'integer',
		'title' => 'string',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
	];
    protected $fillable = [
        'title'
    ];

    public function keywords()
    {
        return $this->hasMany(AnalyticKeyword::class, 'analytic_id')->orderBy('views', 'desc');
    }

    public function groups()
    {
        return $this->hasMany(AnalyticGroup::class, 'analytic_id');
    }
}