<?php

use App\Middleware\Registred;
use App\Middleware\Access;


use App\Modules\TaskManager\Controllers\TaskController;
use App\Modules\TaskManager\Controllers\ProjectController;
use App\Modules\TaskManager\Controllers\SectionController;

$app->group('/admin', function() use($app, $c) {
    $app->get('/projects', ProjectController::class . ':showAdminProjectList')->setName('project.showAdminProjectList')->add(new Access($c, ['projects_view']));
    $app->get('/projects/add', ProjectController::class . ':showAdminProjectAdd')->setName('project.showAdminProjectAdd')->add(new Access($c, ['projects_create']));
    $app->get('/projects/{id}', ProjectController::class . ':showAdminProjectEdit')->setName('project.showAdminProjectEdit')->add(new Access($c, ['projects_update']));

    $app->get('/projects/{id}/tasks', TaskController::class . ':showAdminTaskList')->setName('task.showAdminTaskList')->add(new Access($c, ['tasks_view']));
    $app->get('/tasks/add', TaskController::class . ':showAdminTaskAdd')->setName('task.showAdminTaskAdd')->add(new Access($c, ['tasks_create']));
    $app->get('/tasks/{id}', TaskController::class . ':showAdminTaskEdit')->setName('task.showAdminTaskEdit')->add(new Access($c, ['tasks_update']));
})->add(new Registred($c));

$app->group('/api', function() use($app, $c) {

    $app->post('/projects', ProjectController::class.':createProject')->setName('project.createProject')->add(new Access($c, ['projects_create']));
    $app->post('/projects/{id}', ProjectController::class.':updateProject')->setName('project.updateProject')->add(new Access($c, ['projects_update']));
    $app->get('/projects/{id}/delete', ProjectController::class.':deleteProject')->setName('project.deleteProject')->add(new Access($c, ['projects_delete']));

    $app->post('/projects/{id}/tasks', TaskController::class.':createTask')->setName('task.createTask')->add(new Access($c, ['tasks_create']));
    $app->post('/projects/{id}/add-section', SectionController::class.':createSection')->add(new Access($c, ['tasks_create']));

    $app->post('/tasks/change-position', TaskController::class.':changePosition');
    $app->post('/section/change-position', SectionController::class.':changePosition');
    $app->delete('/section/{id}', SectionController::class.':deleteSection');

    $app->post('/tasks/create-tag', TaskController::class.':createTag')->add(new Access($c, ['tasks_update']));

    $app->post('/tasks/{id}', TaskController::class.':updateTask')->setName('task.updateTask')->add(new Access($c, ['tasks_update']));

    $app->post('/tasks/{id}/change-title', TaskController::class.':changeTitle')->setName('task.changeTitle')->add(new Access($c, ['tasks_update']));
    $app->post('/tasks/{id}/change-description', TaskController::class.':changeDescription')->setName('task.changeDescription')->add(new Access($c, ['tasks_update']));

    $app->post('/tasks/{id}/add-tag', TaskController::class.':addTag')->add(new Access($c, ['tasks_update']));
    $app->post('/tasks/{id}/remove-tag', TaskController::class.':removeTag')->add(new Access($c, ['tasks_update']));

    $app->post('/tasks/{id}/add-file', TaskController::class.':addFile')->add(new Access($c, ['tasks_update']));
    $app->post('/tasks/{id}/remove-file', TaskController::class.':removeFile')->add(new Access($c, ['tasks_update']));

    $app->post('/tasks/{id}/add-user', TaskController::class.':addUser')->add(new Access($c, ['tasks_update']));
    $app->post('/tasks/{id}/remove-user', TaskController::class.':removeUser')->add(new Access($c, ['tasks_update']));

    $app->get('/tasks/{id}/delete', TaskController::class.':deleteTask')->setName('task.deleteTask')->add(new Access($c, ['tasks_delete']));

})->add(new Registred($c));