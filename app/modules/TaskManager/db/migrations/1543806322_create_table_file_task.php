<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_file_task_1543806322 {
    public function up() {
        Capsule::schema()->create('file_task', function( \Illuminate\Database\Schema\Blueprint $table) {
            $table->integer('file_id');
            $table->integer('task_id');
        });
    }

    public function down() {
    	Capsule::schema()->dropIfExists('file_task');
    }
}
