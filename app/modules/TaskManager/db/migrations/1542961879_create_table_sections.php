<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_sections_1542961879 {
    public function up() {
        Capsule::schema()->create('sections', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('position');
            $table->string('title');
        });
    }

        public function down() {
        Capsule::schema()->dropIfExists('sections');
    }
}
