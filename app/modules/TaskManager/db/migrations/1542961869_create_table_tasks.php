<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_tasks_1542961869 {
    public function up() {
        Capsule::schema()->create('tasks', function( \Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('section_id')->nullable();
            $table->integer('position');
            $table->string('title');
            $table->text('description')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down() {
        Capsule::schema()->dropIfExists('tasks');
    }
}
