<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_task_user_1543806415 {
    public function up() {
        Capsule::schema()->create('task_user', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->integer('task_id');
            $table->integer('user_id');
        });       
    }

    public function down() {
    	Capsule::schema()->dropIfExists('task_user');
    }
}
