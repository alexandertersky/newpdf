<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_projects_1542961886 {
    public function up() {
        Capsule::schema()->create('projects', function($table) {
            $table->increments('id');
            $table->string('title');
        });
    }

        public function down() {
        Capsule::schema()->dropIfExists('projects');
    }
}
