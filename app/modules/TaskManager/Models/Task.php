<?php

namespace App\Modules\TaskManager\Models;

use App\Models\CustomModel;
use App\Models\File;
use App\Models\User;

class Task extends CustomModel
{
    public $timestamps = true;
    protected $guarded = [];
    protected $table = 'tasks';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'id' => 'integer',
	];

    public function section()
    {
        return $this->hasOne(Section::class, 'id', 'section_id');
    }

    public function tags()
    {
        return $this->morphToMany("App\Models\Tag", "taggable");
    }

    public function files()
    {
        return $this->belongsToMany(File::class, 'file_task', 'task_id', 'file_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'task_user', 'task_id', 'user_id');
    }

}