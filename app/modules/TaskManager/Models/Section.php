<?php

namespace App\Modules\TaskManager\Models;

use App\Models\CustomModel;

class Section extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'sections';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];


    public function tasks()
    {
        return $this->hasMany(Task::class, 'section_id', 'id');
    }
}