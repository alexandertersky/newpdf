<?php

namespace App\Modules\TaskManager\Models;

use App\Models\CustomModel;

class Project extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'projects';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'id' => 'integer',
		'title' => 'string',
	];

    public function sections()
    {
        return $this->hasMany(Section::class,'project_id','id');
    }

    public function tasks()
    {
        return $this->hasMany(Task::class,'project_id','id');
    }

}