<?php

namespace App\Modules\TaskManager\Controllers;

use App\Controllers\Controller;

use App\Modules\TaskManager\Models\Section;

class SectionController extends Controller
{
    public function createSection($request, $response, $args)
    {
        $data = $request->getParams();

        $section['title'] = $data['title'];
        $section['position'] = $data['position'];
        $section['project_id'] = $args['id'];

        $section = Section::create($section)->load('tasks')->toJson();

        return $section;
    }

    public function changePosition($request, $response, $args)
    {
        $data = $request->getParams()['sections'];
        foreach ($data as $key=>$value)
        {
           Section::find($value['id'])->update(['position' => $value['position']]);
        }
        return json_encode($data);
    }

    public function deleteSection($request, $response, $args)
    {
        $tasks = Section::with('tasks')->find($args['id'])->tasks;
        foreach($tasks as $task){
            $task->section_id = null;
            $task->save();
        }
        Section::destroy($args['id']);
    }
}