<?php
namespace App\Modules\TaskManager\Controllers;

use App\Controllers\Controller;

use App\Modules\TaskManager\Models\Project;
use App\Modules\TaskManager\Models\Task;

use App\Models\Tag;
use App\Models\User;

class TaskController extends Controller
{
    public function showAdminTaskList($request, $response, $args)
    {
        $project = Project::with([
            'tasks' => function($query){
                $query->with('tags', 'files', 'users')->orderBy('position', 'desc');
            },
            'sections' => function($query){
                $query->with([
                    'tasks' => function($query){
                    $query->with('tags', 'files', 'users')->orderBy('position', 'desc');
                }
                ])->orderBy('position', 'desc');
            }
            ])->find($args['id']);

        $tasks = $project->tasks;
        $tasks = $tasks->groupBy('section_id')->toArray();

        $project = $project->toArray();

        $without_section = [
            'position' => 0,
            'tasks' => isset($tasks[''])?$tasks['']:[]
        ];

        array_unshift($project['sections'], $without_section);

        if (count($project['sections']) == 0){
            $this->twig_vars['sections'] = json_encode([ 0 => $project['tasks']]);
        }else{
            $this->twig_vars['sections'] = json_encode($project['sections']);
        }

        $this->twig_vars['project'] = $project;
        $this->twig_vars['tags'] = Tag::get()->toJson();
        $this->twig_vars['users'] = User::get()->toJson();

        $this-> render('/tasks/tasks-list.twig');
    }

    public function createTask($request, $response, $args)
    {
        $data = $request->getParams();
        $data['project_id'] = $args['id'];
        $task = Task::create($data)->load('tags', 'files', 'users');
        return $task->toJson();
    }

    public function deleteTask($request, $response, $args)
    {
        Task::destroy($args['id']);
    }

    public function changeTitle($request, $response, $args)
    {
        $task = Task::find($args['id']);
        $title = $request->getParams()['title'];
        $task->title = $title;
        $task->save();
    }

    public function changeDescription($request, $response, $args)
    {
        $task = Task::find($args['id']);
        $description = $request->getParams()['description'];
        $task->description = $description;
        $task->save();
    }

    public function changePosition($request, $response, $args)
    {
        $data = $request->getParams();

        if ($data){
            foreach ($data['tasks'] as $task){
                Task::find($task['id'])->update([
                    'position' => $task['position'],
                    'section_id' => $task['section_id'] != ''? $task['section_id']: null
                ]);
            }
        }
        return json_encode(true);
    }

    public function createTag($request, $response, $args)
    {
        $data = $request->getParams();
        $tag = Tag::create($data);
        return $tag->toJson();
    }

    public function addTag($request, $response, $args)
    {
        $data = $request->getParams();
        $task = Task::find($args['id']);
        $task->tags()->attach($data['tag_id']);
    }

    public function removeTag($request, $response, $args)
    {
        $data = $request->getParams();
        $task = Task::find($args['id']);
        $task->tags()->detach($data['tag_id']);
    }

    public function addFile($request, $response, $args)
    {
        if(!empty($_FILES['file']) && $_FILES['file']['size'] > 0)
        {
            $file = $this->_uploadFiles('file')[0];
            $task = Task::find($args['id']);
            $task->files()->attach($file['id']);
            return $task->load('files')->toJson();
        }else{
            return json_encode(false);
        }
    }

    public function removeFile($request, $resonse, $args)
    {
        $data = $request->getParams();
        $task = Task::find($args['id']);
        $task->files()->detach($data['file_id']);
        return $task->load('files')->toJson();
    }

    public function addUser($request, $response, $args)
    {
        $data = $request->getParams();
        $task = Task::find($args['id']);
        $task->users()->attach($data['user_id']);
    }

    public function removeUser($request, $response, $args)
    {
        $data = $request->getParams();
        $task = Task::find($args['id']);
        $task->users()->detach($data['user_id']);
    }

}