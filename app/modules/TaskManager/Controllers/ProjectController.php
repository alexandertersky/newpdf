<?php
namespace App\Modules\TaskManager\Controllers;

use App\Controllers\Controller;
use App\Modules\TaskManager\Models\Project;


class ProjectController extends Controller
{
    public function showAdminProjectList($request, $response, $args)
    {
        $this->twig_vars['projects'] = Project::paginate(50);
        $this-> render('/projects/projects-list.twig');
    }

    public function showAdminProjectEdit($request, $response, $args)
    {
        $this->twig_vars['project'] = Project::find($args['id']);
        $this->render('/projects/project-form.twig');
    }

    public function showAdminProjectAdd($request, $response, $args)
    {
        $this->render('/projects/project-form.twig');
    }

    public function createProject($request, $response, $args)
    {
        $data = $request->getParams();
        Project::create($data['project']);
        return $response->withRedirect($this->ci->router->pathFor('project.showAdminProjectList'));

    }

    public function updateProject($request, $response, $args)
    {
        $data = $request->getParams();
        Project::find($args['id'])->update($data['project']);
        return $response->withRedirect($this->ci->router->pathFor('project.showAdminProjectList'));
    }

    public function deleteProject($request, $response, $args)
    {
        Project::destroy($args['id']);
        return $response->withRedirect($this->ci->router->pathFor('project.showAdminProjectList'));
    }

}