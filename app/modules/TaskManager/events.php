<?php

use App\Helpers\Event;

Event::addListener('render_navigation', function ($event, &$items) {

    $items[] = [
        'title' => 'Менеджер Задач',
        'icon' => 'icon-task',
        'path' => 'project.showAdminProjectList'];
});