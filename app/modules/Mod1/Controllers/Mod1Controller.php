<?php

namespace App\Modules\Mod1\Controllers;

use App\Controllers\Controller;
use App\Models\File;
use App\Models\User;

class Mod1Controller extends Controller
{
    public function test($request, $response, $args)
    {
        $user = User::with(['files' => function ($query) {
            $query->orderBy('position');
        }])->where('email', 'test')->first();
        $this->twig_vars['user'] = $user->toArray();
        $this->render('test/test.twig');
    }


    public function upload($request, $response, $args)
    {
        $data = $request->getParams();
        $user = User::with('files')->where('email', 'test')->first();
        $positions = json_decode($data['position'], true);

        //Загружаем полученные файлы
        $files = [];
        if (!empty($_FILES)) {
            foreach ($_FILES["files"]["error"] as $key => $error) {
                if ($error == UPLOAD_ERR_OK) {
                    $file = $this->_uploadFiles('files', '/files', User::class, $user->id);
                    $files[] = $file;
                }
            }
        }

        $user_files_id = [];
        // Обновляем позиции файлов
        foreach ($positions as $key => $position) {
            if (isset($position['id'])) {
                $user_files_id[] = $position['id'];
                File::find($position['id'])->update(['position' => $key + 1]);
            } else {
                File::find($files[0][$position['index'] - 1]['id'])->update(['position' => $key + 1]);
            }
        }


        $user->load(['files' => function ($query) {
            $query->orderBy('position', 'asc');
        }]);

        return $user->files->toJson();
    }

    public function delete($request, $response, $args)
    {
        $data = $request->getParams();
        File::destroy($data['id']);
    }
}