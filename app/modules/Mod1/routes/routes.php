<?php

use App\Modules\Mod1\Controllers\Mod1Controller;

$app->get('/admin/uploader', Mod1Controller::class.':test')->setName('mod.test');

$app->post('/api/uploader', Mod1Controller::class.':upload');

$app->delete('/api/uploader', Mod1Controller::class.':delete');