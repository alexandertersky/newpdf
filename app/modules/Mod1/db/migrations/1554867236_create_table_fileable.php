<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_fileable_1554867236 {
    public function up() {
        Capsule::schema()->create('fileable', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->integer('file_id');
			$table->integer('fileable_id');
			$table->string('fileable_type');
        });
    }

    public function down() {
    	Capsule::schema()->dropIfExists('fileable');
    }
}
