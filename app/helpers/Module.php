<?php


namespace App\Helpers;

use Illuminate\Database\Capsule\Manager as Capsule;
use Composer\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput as Output;
use Symfony\Component\Console\Output\OutputInterface;

class Module
{
    private static $container;
    private $module;
    private static $max_depth = 3;

    public function __construct($container, $module)
    {
        self::$container = $container;
        $this->module = $module;
    }

    public function getTitle()
    {
        return $this->module->convention->ModuleName;
    }

    public function getDescription()
    {
        return $this->module->convention->Description;
    }

    public function getPath()
    {
        return $this->module->path;
    }

    public function getConvention()
    {
        return $this->module->convention;
    }

    public function getStyle(string $filename)
    {
        $styles = $this->getStyles();

        foreach ($styles as $style) {
            if (strpos($style, $filename)) return file_get_contents(realpath($style));
        }

        return false;
    }

    public function getStyles()
    {
        $files = $this->getFiles($this->getPath() . '/assets');

        return isset($files['css']) ? $files['css'] : [];
    }

    public function getScript(string $filename)
    {
        $scripts = $this->getScripts();

        foreach ($scripts as $script) {
            if (strpos($script, $filename)) return file_get_contents(realpath($script));
        }

        return false;
    }

    public function getScripts()
    {
        $files = $this->getFiles($this->getPath() . '/assets');

        return isset($files['js']) ? $files['js'] : [];
    }

    public function getImage(string $filename)
    {
        $images = $this->getImages();
        $extention = pathinfo($filename, PATHINFO_EXTENSION);

        if (isset($images[$extention])) {
            foreach ($images[$extention] as $image) {
                $basename = basaname($image);

                if ($basename == $filename) return file_get_contents(realpath($image));
            }
        }

        return false;
    }

    public function getImages()
    {
        $files = $this->getFiles($this->getPath() . '/assets');
        $images= array_intersect_key($files, array_flip(['jpg', 'jpeg', 'png', 'bmp', 'gif', 'ico']));

        return $images;
    }

    public function getRoutes()
    {
        $routes = $this->getFiles($this->getPath() . '/routes');

        return isset($routes['php']) ? $routes['php'] : [];
    }

    public function getTwigExtensions()
    {
        $extensions = self::getFiles($this->getPath() . '/twig');

        return isset($extensions['php']) ? $extensions['php'] : [];
    }

    public function getFiles(string $path, $depth = 1)
    {
        $getFiles = function($path, $depth = 1) use(&$getFiles) {
            $entities = self::getDirFiles($path);

            $files = [];

            foreach ($entities as $entity) {
                $entity_path = $path . '/' . $entity;

                if (is_dir($entity_path) && $depth < self::$max_depth) {
                    $searched = $getFiles($entity_path, $depth + 1);
                    $files = array_merge_recursive($files, $searched);
                } else {
                    $extension = pathinfo($entity_path, PATHINFO_EXTENSION);
                    $files[$extension][] = $entity_path;
                }
            }

            return $files;
        };

        return $getFiles($path, $depth);
    }

    public function install()
    {
        $this->migrate();

        Capsule::table('modules')->insert([
            'title' => $this->getTitle(),
            'status' => 0,
            'path' => $this->getPath(),
            'convention' => json_encode($this->getConvention())
        ]);

        if (file_exists($this->getPath() . '/composer.json')) {
            $this->updateComposer();
        }
    }

    public function uninstall()
    {
        $this->migrate(false);

        Capsule::table('modules')->where('title', $this->getTitle())->delete();
    }

    public function run()
    {
        $loader = self::$container['view']->getLoader();
        $loader->addPath($this->getPath() . '/views');

        $extensions = $this->getTwigExtensions();

        foreach ($extensions as $extension) {
            $namespace = self::getNamespace($extension);
            $class = $namespace . '\\' . basename($extension, '.php');
            self::$container['view']->addExtension(new $class(self::$container));
        }

        if (file_exists($this->getPath() . '/events.php')) {
            include_once $this->getPath() . '/events.php';
        }
    }

    private function updateComposer()
    {
        $mkdir = function ($dir) {
            if (!file_exists($this->getPath() . '/' . $dir)) {
                mkdir($this->getPath() . '/' . $dir);
            }
        };

        $mkdir('vendor');
        $mkdir('cache');

        ini_set('memory_limit', '1G');
        set_time_limit(300); // 5 minutes execution

        chdir($this->getPath());
        putenv('COMPOSER_HOME=' . $this->getPath());
        putenv('COMPOSER_VENDOR_DIR=' . $this->getPath() . '/vendor');
        putenv('COMPOSER_CACHE_DIR=' . $this->getPath() . '/cache');

        $isDebug = true;
        $output = new Output(
            $isDebug ? OutputInterface::VERBOSITY_DEBUG : OutputInterface::VERBOSITY_NORMAL
        );

        $input = new ArrayInput(['command' => 'update']);
        $application = new Application();
        $application->setAutoExit(false);
        $application->run($input, $output);
    }

    public static function array_merge_recursive_distinct(array &$array1, array &$array2)
    {
        $merged = $array1;

        foreach ( $array2 as $key => &$value )
        {
            if ( is_array ( $value ) && isset ( $merged [$key] ) && is_array ( $merged [$key] ) )
            {
                $merged [$key] = self::array_merge_recursive_distinct($merged [$key], $value);
            }
            else
            {
                $merged [$key] = $value;
            }
        }

        return $merged;
    }

    private static function getNamespace(string $filepath)
    {
        $path = dirname(__DIR__) . '/modules';
        $path = str_ireplace($path, '', $filepath);
        $path = str_ireplace('/', ' ', dirname($path));
        $namespace = str_ireplace(' ', '\\', ucwords($path));

        return 'App\Modules' . $namespace;
    }

    private static function getDirFiles(string $path)
    {
        $files = [];

        if (is_dir($path)) {
            $files = scandir($path);
            $files = count($files) > 2 ? array_slice($files, 2) : [];
        }

        return $files;
    }

    private function migrate($install = true)
    {
        $path =  $this->getPath() . '/db/migrations';
        $migrations = self::getDirFiles($path);

        foreach ($migrations as $migration) {
            include_once $path . '/' . $migration;

            $filename = explode('_', basename($migration, '.php'));
            $time = intval(array_shift($filename));
            $filename[] = $time;
            $class = implode('_', $filename);

            $obj = new $class;

            if ($install) {
                $obj->up();
                Capsule::table('migrations')->insert([
                    'filename' => basename($migration, '.php'),
                    'time' => $time,
                    'class' => $class
                ]);
            } else {
                $obj->down();
                Capsule::table('migrations')->where('filename', basename($migration, '.php'))->delete();
            }
        }
    }
}