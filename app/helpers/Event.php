<?php

namespace App\Helpers;

class Event
{
    private static $emitter;
    const GLOBAL_EVENT = '*';
    /**
     * @var callable[][]
     */
    private static $listeners = [];

    public static function addListener($event, callable $listener)
    {
        self::$listeners[$event][spl_object_hash((object)$listener)] = $listener;
    }

    public static function trigger($event, &$var = null)
    {
        if ($event === self::GLOBAL_EVENT) {
            throw new \Exception('Cannot specifically trigger global event.');
        }
        foreach (self::getListeners($event) as $listener) {
            $listener($event, $var);
        }
    }

    protected static function getListeners($event)
    {
        $listeners = isset(self::$listeners[$event]) ? self::$listeners[$event] : [];
        $globalListeners = isset(self::$listeners[self::GLOBAL_EVENT]) ? self::$listeners[self::GLOBAL_EVENT] : [];
        return array_merge($listeners, $globalListeners);
    }
}