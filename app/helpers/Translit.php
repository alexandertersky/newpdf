<?php


namespace App\Helpers;


class Translit
{
    public static function make($str, $lang = 'ru')
    {
        $str = mb_strtolower($str);

        switch ($lang){
            case 'ru':
                return self::transliterateRu($str);
            default:
                return $str;
        }
    }

    private static function transliterateRu($string)
    {
        $string = mb_strtolower($string);
        $russian = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'];
        $english = ['a', 'b', 'v', 'g', 'd', 'e', 'yo', 'zh', 'z', 'i', '{i_short}', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'kh', 'ts', 'ch', 'sh', 'sch', '{hard_sign}', '{untranslatable}', '', 'e', 'yu', 'ya'];

        // меняем буквы на аналоги или специальные ключи
        $string = str_replace($russian, $english, $string);

        //меняем {hard_sign} (ъе -> ye)
        while (strpos($string, '{hard_sign}')) {
            $pos = strpos($string, '{hard_sign}');
            if (substr($string, $pos + 11, 1) === 'e') {
                $string = substr_replace($string, 'y', $pos, 11);
            } else {
                $string = substr_replace($string, '', $pos, 11);
            }
        }

        //меняем {untranslatable} (ый → iy)
        while (strpos($string, '{untranslatable}')) {
            $pos = strpos($string, '{untranslatable}');
            if (substr($string, $pos + 16, 9) === '{i_short}') {
                $string = substr_replace($string, 'i', $pos, 16);
            } else {
                $string = substr_replace($string, 'y', $pos, 16);
            }
        }

        $string = str_replace('{i_short}', 'y', $string);

        return $string;
    }
}