<?php


namespace App\Helpers;

final class ModulesSingleton
{
    private static $instance;
    private static $container;

    private static $modules_dirrectory;
    private static $modules;

    private function __construct($container) {
        self::$modules_dirrectory = __DIR__ . '/../modules';
        self::$container = $container;
    }

    private function __clone() {}
    private function __wakeup() {}

    public static function getInstance($container = null): ModulesSingleton
    {
        if (null === static::$instance) {
            static::$instance = new static($container);
        }

        return static::$instance;
    }

    /**
     * Get list of modules with description
     *
     * @return array
     */
    public static function getModules()
    {
        if (self::$modules) return self::$modules;

        $folders = self::getDirFiles(self::$modules_dirrectory);

        self::$modules = [];
        foreach ($folders as $folder) {
            $path = realpath(self::$modules_dirrectory . '/' . $folder);

            if (is_dir($path) === false) continue;

            $convention_path = $path . '/convention.json';

            if (file_exists($convention_path)) {
                $module = new \stdClass;
                $module->convention = json_decode(file_get_contents($convention_path));
                $module->path = $path;

                self::$modules[$module->convention->ModuleName] = $module;
            }
        }

        return self::$modules;
    }

    public static function getModule(string $module_name)
    {
        $mods = self::getModules();

        if (!isset($mods[$module_name])) {
            return null;
            //throw new \Exception(sprintf('Модуль %s не найден', $module_name));
        }

        return new Module(self::$container, $mods[$module_name]);
    }

    /**
     * Get files list by path
     *
     * @param string $path
     * @return array|false
     */
    private static function getDirFiles(string $path)
    {
        $files = [];

        if (is_dir($path)) {
            $files = scandir($path);
            $files = count($files) > 2 ? array_slice($files, 2) : [];
        }

        return $files;
    }
}