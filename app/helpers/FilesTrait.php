<?php


namespace App\Helpers;


use App\Models\File;
use App\Controllers\Controller;

trait FilesTrait
{
    public function checkFiles($input_name, $uploader_data)
    {
        $positions = json_decode($uploader_data['positions'], true);
        $remove_list = json_decode($uploader_data['remove'], true);
//        ddd($positions);
        // Удаляем файлы из базы
        if (count($remove_list) > 0){
            File::destroy($remove_list);
        }

        //Загружаем полученные файлы
        $files = [];
        if (!empty($_FILES)) {
            $files = Controller::__uploadFiles($input_name, '/files', get_class($this), $this->id);
        }

        $user_files_id = [];
        // Обновляем позиции файлов
        foreach ($positions as $key => $position) {
            if (isset($position['id'])) {
                $user_files_id[] = $position['id'];
                File::find($position['id'])->update(['position' => $key + 1]);
            } else {
                foreach ($files as $file){
                    if ($file['filename'] == $position['filename']){
                        File::find($file['id'])->update(['position' => $key + 1]);
                        break;
                    }
                }
            }
        }
        return $this;
    }
}