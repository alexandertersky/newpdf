<?php


namespace App\Helpers;


trait Alias
{
//    Define this property in your model class
//    public $alias_settings = [
//        'source' => 'title',
//        'destination' => 'alias',
//        'delimiter' => '-',
//        'maxlength' => 200,
//        'on' => 'saving' (saving, creating, etc.)
//    ];

    public static function bootAlias()
    {
        switch (self::$alias_settings['on']) {
            case 'saving':
                static::saving(function ($model) {
                    $model->setAttribute(self::$alias_settings['destination'], self::generateAlias($model));
                });
                break;
            case 'creating':
                static::creating(function ($model) {
                    $model->setAttribute(self::$alias_settings['destination'], self::generateAlias($model));
                });
        }
    }

    /**
     * @param $model
     */
    private static function generateAlias($model)
    {
        $src = $model->{self::$alias_settings['source']};
        $separator = self::$alias_settings['separator'];

        $alias = self::makeSlug($src, $separator);

        $alias = self::checkAliasExists($model, $alias, self::$alias_settings['destination'], $separator);

        return $alias;
    }

    private static function checkAliasExists($model, $alias, $column, $separator)
    {
        $sameAliases = get_class($model)::where(
            [
                [$column, $alias],
                ['id', '!=', $model->id]
            ]
        )->orderBy($column)->get();

        //Если существует похожий псевдоним
        if ($sameAliases->count() > 0) {
            $alias .= $separator . get_class($model)::where([[$column, 'like', '%' . $alias . '%'], ['id', '!=', $model->id]])->get()->count();
        }

        return $alias;
    }

    private static function makeSlug($src, $separator)
    {
        $slug = Translit::make($src);

        $slug = preg_replace('![^\pL\pN\s]+!u', '', $slug);
        $slug = str_replace(' ', $separator, $slug);
        $slug = substr($slug, 0, 200);

        return self::checkLastSymbol($slug);
    }

    private static function checkLastSymbol($str){
        $last_symbol = substr($str, -1, 1);
        if (preg_match('~\w~', $last_symbol) == 1){
            return $str;
        }
        return self::checkLastSymbol(substr($str, 0, -1));
    }
}