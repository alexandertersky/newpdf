<?php


namespace App\Helpers;


class Cache
{
    public static function set($key, $value, $ttl = 3600)
    {
        self::writeCacheFile($key, $value, $ttl);
    }

    public static function get($key)
    {
        $cache = self::readCacheFile();

        if ($cache && isset($cache[$key]) && $cache[$key]['ttl'] > time()) {
            return $cache[$key]['value'];
        }

        return null;
    }

    public static function setMultiple(array $values, $ttl = 3600)
    {
        foreach ($values as $key => $value) {
            self::set($key, $value, $ttl);
        }
    }

    public static function getMultiple(array $keys)
    {
        $cache = [];

        foreach ($keys as $key) {
            $cache[] = self::get($key);
        }

        return $cache;
    }

    private static function readCacheFile()
    {
        if (file_exists(realpath($_SERVER['DOCUMENT_ROOT']) . '/cache/_cache')) {
            $cache = file_get_contents(realpath($_SERVER['DOCUMENT_ROOT']) . '/cache/_cache');

            return (array)json_decode($cache, true);
        }

        return null;
    }

    private static function writeCacheFile($key, $value, $ttl)
    {
        if (!file_exists(realpath($_SERVER['DOCUMENT_ROOT']) . '/cache')) {
            mkdir(realpath($_SERVER['DOCUMENT_ROOT']) . '/cache', 0777, true);
        }

        $cache = self::readCacheFile();
        $cache = $cache ? $cache : [];

        $cache[$key] = [
            'value' => $value,
            'ttl' => $ttl + time()
        ];

        file_put_contents(realpath($_SERVER['DOCUMENT_ROOT']) . '/cache/_cache', json_encode($cache));
    }
}