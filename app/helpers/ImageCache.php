<?php
namespace App\Helpers;

class ImageCache
{
    public $style;
    public $image_manager;
    public $img_original;
    public $style_path;

    public function __construct()
    {
        $configPath = __DIR__.'/../../configs/styles.yaml';
        $styles = \Spyc::YAMLLoad($configPath);

        if (!isset($styles[$_GET['preset']])) {
            throw new \Exception('Нет такого стиля для изображения в конфиге');
        } else {
            $this->style = $styles[$_GET['preset']];
        }

        $this->image_manager  = new \Intervention\Image\ImageManagerStatic;
        $this->img_original = __DIR__.'/../../public/'.$_GET['file'];
        $this->style_path = $_GET['preset'].'/'.$_GET['file'];
    }

    public function process()
    {
        $this->_createFolderIfNotExist();
        $img = $this->image_manager->make($this->img_original);
        foreach ($this->style as $action) {
            $operation = $action['operation'];
            if (isset($action['functions']) && $action['functions'] == 'aspectRatio') {
                $width = $action['parametres'][0];
                $height = $action['parametres'][1];
                $img->$operation($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            } else {
                $img->$operation(...$action['parametres']);
            }
        }

        // Только для локальной разработки
        if (getenv('APP_ENV') == 'local') {
            $img->save(__DIR__.'/../../public/imagecache/'.$this->style_path);
        } else {
            $img->save(__DIR__.'/../../../../shared/public/imagecache/'.$this->style_path);
        }
        echo $img->response();
    }

    private function _createFolderIfNotExist()
    {
        $structure = explode('/', $this->style_path);
        array_pop($structure);
        $structure = implode('/', $structure);
        // Только для локальной разработки
        if (getenv('APP_ENV') == 'local') {
            $folder = __DIR__.'/../../public/imagecache/'.$structure;
        } else {
            $folder = __DIR__.'/../../../../shared/public/imagecache/'.$structure;
        }

        if (!is_dir($folder)) {
            mkdir($folder, 0777, true);
        }
    }

}



