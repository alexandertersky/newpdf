<?php

namespace App\Helpers;

use App\Models\NavigationItem;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigCustomExtensions extends AbstractExtension
{
    public function __construct($container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return array(
            new TwigFunction('getNavigation', array($this, 'getNavigation')),
            new TwigFunction('getAdminNavigation', array($this, 'getAdminNavigation')),
            new TwigFunction('showPaginate', array($this, 'showPaginate')),
        );
    }

    public function getAdminNavigation()
    {
        $items = file_get_contents(__DIR__.'/../../configs/admin_navigation.json');
        $items = json_decode($items, true);
        return $items;
    }

    public function getNavigation($menu_id)
    {
        $user = $this->container['sentinel']->check();
        $items = NavigationItem::where('navigation_id', $menu_id)
            ->where('parent_id', null)
            ->with('children')
            ->orderBy('position')
            ->get()
            ->toArray();

        Event::trigger('render_navigation', $items);

        $checkAccess = function (&$items) use (&$checkAccess, $user) {
            foreach ($items as $key => $item) {
                if ($this->hasAccess($user, $item) === false) unset($items[$key]);
                else if (isset($item['children'])) $checkAccess($item['children']);
            }
        };

        //$checkAccess($items);

        $len = 0;

        $setActiveItem = function (&$items) use (&$setActiveItem, &$len) {
            foreach ($items as $key => &$item) {
                $path = $this->container->router->pathFor($item['path']);
                $pos = strripos($_SERVER['REQUEST_URI'], $path);

                if ($pos === 0 && strlen($path) > $len) {
                    $len = strlen($path);
                    $activeChild = isset($item['children']) && $setActiveItem($item['children']);

                    if ($activeChild == false) $item['class'] = 'active';

                    return true;
                }
            }
            return false;
        };

        $setActiveItem($items);

        return $items;

    }

    public function hasAccess($user, $item)
    {
        if (!isset($item['permissions']) || !$item['permissions']) return true;

        $permissions = json_decode($item['permissions']);

        $permissions_arr = [];
        foreach ($permissions as $permission_name => $value) {
            if ($value == 'true') {
                array_push($permissions_arr, $permission_name);
            }
        }

        if (!$user->hasAccess($permissions_arr)) {
            unset($item);
        }
    }

    /**
     * @param $objects - массив или объект для пагинации
     * @return string - html разметка пагинации
     */
    public function showPaginate($objects)
    {
        $html = '';
        if (is_array($objects)) {
            if ($objects['total'] > $objects['per_page']) {
                if ($objects['current_page'] > 1) {
                    $html .= '<li class="page-item">
                                    <a class="page-link" href="?' . $this->getDataFromUrl() . 'page=1">&lt;&lt;В начало</a>
                                </li>';
                }
                for ($i = 3; $i >= 1; $i--) {
                    if ($objects['current_page'] - $i > 0) {
                        $number = $objects['current_page'] - $i;
                        $html .= '<li class="page-item">
                                    <a class="page-link" href="?' . $this->getDataFromUrl() . 'page=' . $number . '">' . $number . '</a>
                                  </li>';
                    }
                }
                $html .= '<li class="page-item active">
                                <a href="" class="page-link active-link">' . $objects['current_page'] . '</a>
                            </li>';
                for ($i = 1; $i <= 3; $i++) {
                    if ($objects['current_page'] + $i < $objects['last_page']) {
                        $number = $objects['current_page'] + $i;
                        $html .= '<li class="page-item">
                                    <a class="page-link" href="?' . $this->getDataFromUrl() . 'page=' . $number . '">' . $number . '</a>
                                  </li>';
                    }
                }
                if ($objects['current_page'] < $objects['last_page']) {
                    $html .= '<li class="page-item">
                                    <a class="page-link" href="?' . $this->getDataFromUrl() . 'page=' . $objects['last_page'] . '">В конец&gt;&gt;</a>
                                </li>';
                }
            }
        } else {
            if ($objects->total() > $objects->perPage()) {
                if ($objects->currentPage() > 1) {
                    $html .= '<li class="page-item">
                                    <a class="page-link" href="?' . $this->getDataFromUrl() . 'page=1">&lt;&lt;В начало</a>
                                </li>';
                }
                for ($i = 3; $i >= 1; $i--) {
                    if ($objects->currentPage() - $i > 0) {
                        $number = $objects->currentPage() - $i;
                        $html .= '<li class="page-item">
                                    <a class="page-link" href="?' . $this->getDataFromUrl() . 'page=' . $number . '">' . $number . '</a>
                                  </li>';
                    }
                }
                $html .= '<li class="page-item active">
                                <a href="" class="page-link active-link">' . $objects->currentPage() . '</a>
                            </li>';
                for ($i = 1; $i <= 3; $i++) {
                    if ($objects->currentPage() + $i < $objects->lastPage()) {
                        $number = $objects->currentPage() + $i;
                        $html .= '<li class="page-item">
                                    <a class="page-link" href="?' . $this->getDataFromUrl() . 'page=' . $number . '">' . $number . '</a>
                                  </li>';
                    }
                }
                if ($objects->currentPage() < $objects->lastPage()) {
                    $html .= '<li class="page-item">
                                    <a class="page-link" href="?' . $this->getDataFromUrl() . 'page=' . $objects->lastPage() . '">В конец&gt;&gt;</a>
                                </li>';
                }
            }
        }
        return '<nav class="row justify-content-center">
                    <ul class="pagination">
                        ' . $html . '
                    </ul>
                </nav>
            ';
    }

    function getDataFromUrl()
    {
        $urlParams = '';
        foreach ($_REQUEST as $key => $value) {
            if ($key != 'page') {
                $urlParams .= $key . '=' . $value . '&';
            }
        }
        return $urlParams;
    }
}