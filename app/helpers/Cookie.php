<?php


namespace App\Helpers;


class Cookie
{
    public static function delete($name)
    {
        setcookie($name, null, -2147483647, '/');
    }

    public static function set($name, $value, $time = 2592000)
    {
        setcookie($name, $value, time() + $time, '/');
    }

    public static function addToArray($name, $value, $time = 2592000)
    {
        if (!isset($_COOKIE[$name])) {
            setcookie($name . '[]', $value, time() + $time, '/');
        } else {
            $arr = $_COOKIE[$name];
            setcookie($name . '[' . count($arr) . ']', $value, time() + $time, '/');
        }
        return true;
    }

    public static function addToArrayUnique($name, $value, $time = 2592000)
    {
        if (!isset($_COOKIE[$name])) {
            setcookie($name . '[]', $value, time() + $time, '/');
        } else {
            $arr = $_COOKIE[$name];
            if (!in_array($value, $arr)){
                setcookie($name . '[' . count($arr) . ']', $value, time() + $time, '/');
                return true;
            }
            return false;
        }
        return true;
    }
}