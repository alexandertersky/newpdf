<?php

namespace App\Helpers;

use App\Models\Module;

class ModuleLoader
{
    public static function getModulesPath()
    {
        $modules = Module::where('status', '1')->get()->toArray();
        $path = [];
        foreach ($modules as $module)
        {
            $dir_name = end(explode('/', $module['path']));
            $path[] =  __DIR__.'/../modules/'.$dir_name;
        }

        return $path;
    }

    public static function allFilesPath()
    {
        $array_path = [];
        foreach (self::getModulesPath() as $path)
        {
            if (file_exists($path.DIRECTORY_SEPARATOR.'autoload.php'))
            {
                array_push($array_path, $path.DIRECTORY_SEPARATOR.'autoload.php');
                continue;
            }

            $dir_info = scandir($path);
            array_shift($dir_info);
            array_shift($dir_info);

            $result = false;
            while($result == false)
            {
                foreach($dir_info as $key=>$item)
                {
                    if (strripos($item, '.') == false)
                    {
                        $item_info = scandir($path.DIRECTORY_SEPARATOR.$item);
                        array_shift($item_info);
                        array_shift($item_info);
                        foreach ($item_info as $value)
                        {
                            $dir_info[] = $item.DIRECTORY_SEPARATOR.$value;
                        }
                        unset($dir_info[$key]);
                    }
                }

                // Проверка
                foreach ($dir_info as $item)
                {
                    $result = true;
                    if (!strripos($item, '.'))
                    {
                        $result = false;
                        break;
                    }
                }
            }

            foreach ($dir_info as $info)
            {
                array_push($array_path, $path.DIRECTORY_SEPARATOR.$info);
            }
        }

        foreach($array_path as $key=>$path)
        {
            $name = end(explode('/', $path));
            $extension = end(explode('.', $name));
            if (!in_array($extension, ['php']))
            {
                unset($array_path[$key]);
            }
        }

        return $array_path;
    }

    public static function getViewsPath()
    {
        $arr = [];
        foreach (self::getModulesPath() as $path)
        {
            if (file_exists($path.'/views'))
            {
                $arr[]= $path.'/views';
            }
        }
        return  $arr;
    }

}