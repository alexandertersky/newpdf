<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_tags_1535097736 {
    public function up() {
        Capsule::schema()->create('tags', function($table) {
            $table->increments('id');
            $table->string('title');
        });
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
