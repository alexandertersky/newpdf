<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_taggables_1535097670 {
    public function up() {
        Capsule::schema()->create('taggables', function($table) {
            $table->increments('id');
            $table->integer('tag_id');
            $table->integer('taggable_id');
            $table->string('taggable_type');
        });
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
