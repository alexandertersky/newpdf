<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_navigation_items_1541127018 {
    public function up() {
        Capsule::schema()->create('navigation_items', function($table) {
            $table->increments('id');
			$table->integer('navigation_id');
			$table->string('title');
			$table->text('path');
			$table->float('position');
			$table->integer('parent_id')->nullable();
			$table->string('icon')->nullable();
			$table->text('permissions');
			
        });

        Capsule::table('navigation_items')->insert([
			'id' => '1',
			'navigation_id' => '1',
			'title' => 'Пользователи',
			'path' => 'user.showAdminUserList',
			'position' => '1',
			'parent_id' => null,
			'icon' => 'icon-users',
			'permissions' => '{"user_view":"false","user_create":"false","user_update":"false","user_delete":"false","roles_view":"false","modules_view":"false","modules_create":"false","modules_update":"false","modules_delete":"false","navigation_view":"false","navigation_create":"false","navigation_update":"false","navigation_delete":"false"}',
		]);

		Capsule::table('navigation_items')->insert([
			'id' => '2',
			'navigation_id' => '1',
			'title' => 'Навигация',
			'path' => 'navigation.showAdminNavigationList',
			'position' => '3',
			'parent_id' => null,
			'icon' => 'icon-menu3',
			'permissions' => '{"user_view":"false","user_create":"false","user_update":"false","user_delete":"false","roles_view":"false","modules_view":"false","modules_create":"false","modules_update":"false","modules_delete":"false","navigation_view":"false","navigation_create":"false","navigation_update":"false","navigation_delete":"false"}',
		]);

		Capsule::table('navigation_items')->insert([
			'id' => '3',
			'navigation_id' => '1',
			'title' => 'Модули',
			'path' => 'module.showAdminModuleList',
			'position' => '2',
			'parent_id' => null,
			'icon' => 'icon-diamond',
			'permissions' => '{"user_view":"false","user_create":"false","user_update":"false","user_delete":"false","roles_view":"false","modules_view":"false","modules_create":"false","modules_update":"false","modules_delete":"false","navigation_view":"false","navigation_create":"false","navigation_update":"false","navigation_delete":"false"}',
		]);

		
    }

}
