<?php
require_once __DIR__  . '/../../vendor/autoload.php';


//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);

$dotenv = new \Dotenv\Dotenv(dirname(__DIR__.'/../../configs/.env'));
$dotenv->load();


//ddd(getenv('APP_ENVIRONMENT'));
// Только для локальной разработки
if (getenv('APP_ENV') == 'local') {
    require_once __DIR__ . '/../../app/helpers/ImageCache.php';
} else {
    require_once __DIR__ . '/../../../current/app/helpers/ImageCache.php';
}


ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$image = new \App\Helpers\ImageCache();
$image->process();